/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thrones;

/**
 *
 * @author sergk
 */
import java.io.*;
import org.apache.log4j.*;

public class FileReader {

    public static final Logger LOG=Logger.getLogger(FileReader.class);
    
    public static String readPage(String filename, String encoding) 
    {
        
        String str = "";
        String fileString="";
        try
            {
            try {
                FileInputStream fis = new FileInputStream(filename);
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(filename), encoding);
                BufferedReader in = new BufferedReader(isr);
                while (((str = in.readLine()) != null)) {
                    fileString = fileString + "\n" + str;
                }
                in.close();
            } 
            catch (IOException e) 
            {
                LOG.error("Any IO problems: "+e.getMessage());
                return e.getMessage();
            }
           }
        catch (Exception Ex)
        {
          LOG.error("Any big problem: "+Ex.getMessage());  
          return Ex.getMessage();  
        }
        
        LOG.info("File: "+filename+" succesfully readed using encoding "+encoding);
        
        return fileString;
    }
}        