/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.network;

import ru.wnfx.scriptfighter.http.core.*;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import me.legrange.mikrotik.ApiConnection;
import me.legrange.mikrotik.MikrotikApiException;

/**
 *
 * @author sergk
 */
public class Mikrotik extends Library {

    private String secretKey;
    private String apiKey;
    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(Http.class);
    private ApiConnection con;

    public Mikrotik(CommonEngine g) {
        prefix = "Mikrotik";
        desc = "Mikrotik API";

        if (g != null) {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }

    @JavaScriptMethod(type = "interfaces", autocomplete = true, params = "String url", desc = "gets HTTP headers of request")
    public Mikrotik init(String ip, String login, String password) {
        try {
            con = ApiConnection.connect(ip);
            con.login(login, password);

            return this;
        } catch (MikrotikApiException ex) {
            Logger.getLogger(Mikrotik.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @JavaScriptMethod(type = "interfaces", autocomplete = true, params = "String url", desc = "gets HTTP headers of request")
    public Object execute(String command) {
        List<Map<String, String>> result = null;
        try {
            result = con.execute(command);
            con.close();
        } catch (MikrotikApiException ex) {
            Logger.getLogger(Mikrotik.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ObjectUtils.convert(result);
    }

}
