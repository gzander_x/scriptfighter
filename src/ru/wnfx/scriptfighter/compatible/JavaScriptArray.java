/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.compatible;

import java.util.ArrayList;

/**
 *
 * @author sisyandra
 */
//Proxy class for JS Array
public class JavaScriptArray<E> extends ArrayList<E> {
    
    public void push(E entry)
    {
        this.add(entry);
    }
    
    public static JavaScriptArray toJSArray(ArrayList basic)
    {
        JavaScriptArray jsa = new JavaScriptArray();
        jsa.addAll(basic);
        return jsa;
    }
    
}
