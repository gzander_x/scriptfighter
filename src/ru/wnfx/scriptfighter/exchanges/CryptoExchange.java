/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.exchanges;

import ru.wnfx.scriptfighter.http.core.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import java.util.Calendar;
import java.util.Date;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bitfinex.v1.BitfinexExchange;
import org.knowm.xchange.bitfinex.v1.service.BitfinexAccountService.BitfinexFundingHistoryParams;
import org.knowm.xchange.bitfinex.v1.service.BitfinexTradeService.BitfinexTradeHistoryParams;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.account.Balance;
import org.knowm.xchange.dto.account.FundingRecord;
import org.knowm.xchange.dto.account.Wallet;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;
import org.knowm.xchange.service.account.AccountService;
import org.knowm.xchange.service.trade.TradeService;
import org.knowm.xchange.service.trade.params.TradeHistoryParams;

/**
 *
 * @author sergk
 */
public class CryptoExchange extends Library {

    private String secretKey;
    private String apiKey;
    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(Http.class);
    private final String last_url;
    private ExchangeSpecification exSpec;
    private Exchange exchange;

    public CryptoExchange(CommonEngine g) {
        prefix = "CryptoExchange";
        desc = "Crypto API";
        last_url = null;

        if (g != null) {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }

    @JavaScriptMethod(type = "interfaces", autocomplete = true, params = "String exchangeName, String apiKey, String secretKey", desc = "gets HTTP headers of request")
    public CryptoExchange init(String exchangeName, String apiKey, String secretKey) {
        if (exchangeName.equalsIgnoreCase("bitfinex")) {
            exSpec = new BitfinexExchange().getDefaultExchangeSpecification();
            //exSpec.setUserName("34387");
            exSpec.setApiKey(apiKey);
            exSpec.setSecretKey(secretKey);
            exchange = ExchangeFactory.INSTANCE.createExchange(exSpec);
            return this;
        }

        return null;
    }

    @JavaScriptMethod(type = "interfaces", autocomplete = true, params = "String currency_from, String currency_to", desc = "gets HTTP headers of request")
    public Object getTradeHistory(String currency_from, String currency_to) {
        TradeService trade_service = exchange.getTradeService();
        int page_size = 80;
        List<Map<String, Object>> result = new ArrayList();
        Long timestamp_from = Long.valueOf(0);
        Long timestamp_to = Long.valueOf((new Date()).getTime());
        Date dt = new Date();
        dt.setYear(dt.getYear() - 1);
        Long year_ago = (dt).getTime() - 68400 * 1000 * 360;
        Long min_timestamp = null;
        do {
            try {
                CurrencyPair cp = new CurrencyPair(currency_from, currency_to);
                TradeHistoryParams thp = (TradeHistoryParams) new BitfinexTradeHistoryParams(new Date(timestamp_from), new Date(timestamp_to), page_size, cp);

                UserTrades ut = trade_service.getTradeHistory(thp);
                List<UserTrade> trades = ut.getUserTrades();

                if (min_timestamp == null) {
                    min_timestamp = Long.valueOf((new Date()).getTime());
                }
                Long old_min_timestamp = min_timestamp;
                for (UserTrade trade : trades) {
                    Map<String, Object> mp = new HashMap();
                    mp.put("type", trade.getType().toString());
                    mp.put("currency", trade.getCurrencyPair().toString());
                    mp.put("order_id", trade.getOrderId());
                    mp.put("price", trade.getPrice());
                    mp.put("id", trade.getId());
                    mp.put("original_amount", trade.getOriginalAmount());
                    mp.put("timestamp", trade.getTimestamp().getTime());
                    mp.put("date", ru.wnfx.scriptfighter.utils.DateUtils.getString(trade.getTimestamp()));
                    if (min_timestamp > trade.getTimestamp().getTime()) {
                        min_timestamp = trade.getTimestamp().getTime();
                    }

                    mp.put("fee_amount", trade.getFeeAmount());
                    mp.put("fee_currency", trade.getFeeCurrency().toString());
                    result.add(mp);
                }

                if (min_timestamp != old_min_timestamp) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date(min_timestamp));
                    cal.add(Calendar.DATE, -100);
                    timestamp_from = cal.getTimeInMillis();
                    timestamp_to = min_timestamp;
                } else {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date(timestamp_from));
                    cal.add(Calendar.DATE, -100);
                    timestamp_from = cal.getTimeInMillis();

                    cal.setTime(new Date(timestamp_to));
                    cal.add(Calendar.DATE, -100);
                    timestamp_to = cal.getTimeInMillis();
                }

                System.out.println("check dates: " + new Date(timestamp_from) + ", " + new Date(timestamp_to));
                Thread.sleep(4000);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }

        } while (timestamp_from > year_ago && timestamp_from > 0);

        return ObjectUtils.convert(result);
    }

    @JavaScriptMethod(type = "interfaces", autocomplete = true, params = "", desc = "Retrieve full account statistics")
    public Object getAccountData() {
        AccountService accountService = exchange.getAccountService();
        Map<String, Object> result = new HashMap();
        try {
            AccountInfo accountInfo = accountService.getAccountInfo();
            Map<String, Wallet> mp = accountInfo.getWallets();

            for (String waltype : mp.keySet()) {
                List<Map<String, Double>> wallet_result = new ArrayList();
                Wallet wal = mp.get(waltype);
                Map<Currency, Balance> balances = wal.getBalances();
                for (Currency cur : balances.keySet()) {
                    Map<String, Double> balance = new HashMap();
                    balance.put(cur.getCurrencyCode(), balances.get(cur).getTotal().doubleValue());
                    wallet_result.add(balance);
                }
                result.put(waltype, wallet_result);
            }
            System.out.println(mp);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex);
        }

        return ObjectUtils.convert(result);
    }

    @JavaScriptMethod(type = "interfaces", autocomplete = true, params = "String currency", desc = "gets HTTP headers of request")
    public Object getFundingHistory(String currency) {
        AccountService accountService = exchange.getAccountService();
        List<Map<String, Object>> result = new ArrayList();
        do {
            try {
                //TradeHistoryParams params = accountService.createFundingHistoryParams();
                TradeHistoryParams bfp = (TradeHistoryParams) new BitfinexFundingHistoryParams(new Date(0), new Date(), 50, new Currency(currency));
                //TradeHistoryParams thp = (TradeHistoryParams) new BitfinexTradeHistoryParams(new Date(0), 1000, CurrencyPair.BTC_USD);
                List<FundingRecord> fundings = accountService.getFundingHistory(bfp);

                for (FundingRecord fr : fundings) {
                    Map<String, Object> ls = new HashMap();
                    ls.put("currency", fr.getCurrency().toString());
                    ls.put("amount", fr.getAmount());
                    ls.put("balance", fr.getBalance());
                    ls.put("timestamp", fr.getDate().getTime());
                    ls.put("date", ru.wnfx.scriptfighter.utils.DateUtils.getString(fr.getDate()));
                    ls.put("status", fr.getStatus().toString());
                    ls.put("desc", fr.getDescription());
                    ls.put("type", fr.getType().toString());
                    ls.put("fee", fr.getFee());
                    ls.put("address", fr.getAddress());
                    result.add(ls);
                }
                /*Map<String, Wallet> mp = accountInfo.getWallets();

                for(String waltype: mp.keySet())
                {
                    List<Map<String, Double>> wallet_result = new ArrayList();
                    Wallet wal = mp.get(waltype);
                    Map<Currency, Balance> balances = wal.getBalances();
                    for(Currency cur:balances.keySet())
                    {
                        Map<String, Double> balance = new HashMap();
                        balance.put(cur.getCurrencyCode(), balances.get(cur).getAvailable().doubleValue());
                        wallet_result.add(balance);
                    }
                    result.put(waltype, wallet_result);
                }*/
                System.out.println(result);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } while (1 == 0);

        return ObjectUtils.convert(result);
    }
}
