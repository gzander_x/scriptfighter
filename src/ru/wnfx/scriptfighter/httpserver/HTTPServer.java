/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.httpserver;

import ru.wnfx.scriptfighter.interfaces.http.*;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import ru.wnfx.scriptfighter.interfaces.Listener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.PoolExecutor;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;
import sun.org.mozilla.javascript.internal.Undefined;

/**
 *
 * @author sergk
 */
public class HTTPServer extends Listener implements HttpHandler {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(HTTPServer.class);
    private int port;
    private String name;
    private ArrayList<HttpContext> contexts;
    private volatile ArrayBlockingQueue<HTTPServerTask> requests_queue;
    private volatile ArrayBlockingQueue<HTTPResponseTask> response_queue;

    private HttpContext main_context;
    private boolean isModern;
    HttpServer server;
    private PoolExecutor enginePool;
    private volatile ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool;
    private int thread_count = ConfigUtils.getConfigInt("thrones.rest.threads", 3);

    public HTTPServer(Map<String, Object> mappings, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, int port, Scriptable parent, boolean isModern) {
        this.isModern = isModern;
        requests_queue = new ArrayBlockingQueue(1000);
        response_queue = new ArrayBlockingQueue(1000);
        enginePool = new PoolExecutor(thread_count, this, message_queue, commands_queue, events_queue, incoming_events_queue);
        responsePool = new ConcurrentHashMap();
        contexts = new ArrayList();
        //this.event_name = event_name;
        //this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        this.port = port;

        server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(port), 10);
        } catch (IOException ex) {
            LOG.error("Error starting listener with name: " + name + "... " + ex.getMessage());
        }

        HttpHandler mainhandler = new HttpHandler() {
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                Gson gson = new Gson();
                StringWriter sw = new StringWriter();
                JsonWriter jw = new JsonWriter(sw);
                jw.beginObject();
                gson.toJson(Integer.valueOf(100/*get load factor*/), Integer.class, jw.name("load_factor"));
                jw.name("methods").beginArray();
                for (HttpContext c : contexts) {
                    jw.beginObject();
                    gson.toJson(c.getPath().replace("/", ""), String.class, jw.name("name"));
                    gson.toJson(c.getAttributes().get("cacheable"), Boolean.class, jw.name("cacheable"));
                    gson.toJson(c.getAttributes().get("clear_cache"), Boolean.class, jw.name("clear_cache"));
                    Object cache_id = c.getAttributes().get("cache_id");
                    if (cache_id instanceof String) {
                        gson.toJson(c.getAttributes().get("cache_id"), String.class, jw.name("cache_id"));
                    } else if (cache_id instanceof List) {
                        jw.name("cache_id").beginArray();
                        for (String s : (List<String>) cache_id) {
                            jw.value(s);
                        }
                        jw.endArray();
                    }
                    gson.toJson(c.getAttributes().get("impacts"), Boolean.class, jw.name("impacts"));
                    jw.endObject();
                }
                jw.endArray();
                jw.endObject();
                String response = sw.toString();
                jw.flush();
                sw.flush();
                jw.close();
                sw.close();
                exchange.sendResponseHeaders(200, response.length());
                exchange.getResponseBody().write(response.getBytes("UTF-8"));
                exchange.close();
            }
        };

        main_context = server.createContext("/", mainhandler);
        //main_context.getFilters().add(new RestfulFilter(requests_queue, ""));

        for (String webmethod : mappings.keySet()) {
            HttpContext context = server.createContext("/" + webmethod, this);
            Object mappin = ObjectUtils.convert(mappings.get(webmethod));
            if (mappin instanceof String) {
                context.getAttributes().put("cacheable", Boolean.FALSE);
                context.getAttributes().put("clear_cache", Boolean.FALSE);
                context.getFilters().add(new ServerFilter(requests_queue, (String) mappin, webmethod, responsePool));
            } else if (mappin instanceof Map) {
                Object handler = ((Map) mappin).get("handler");
                if (handler instanceof Undefined) {
                    LOG.error("Adding listener for webmethod " + webmethod + "... No handler specified!");
                } else if (handler instanceof String) {
                    Object cach = ((Map) mappin).get("cacheable");
                    if (cach instanceof Undefined) {
                        LOG.error("Adding listener for webmethod " + webmethod + "... No caching policy specified!");
                    } else if (cach instanceof Boolean) {
                        boolean cache_politics = (Boolean) cach;
                        Object cach_id = ObjectUtils.convert(((Map) mappin).get("cache_id"));
                        if (cach_id instanceof Undefined) {
                            LOG.error("Adding listener for webmethod " + webmethod + "... No cache_id specified!");
                        } else if (cach_id instanceof String || cach_id instanceof List) {
                            Object cache_id = cach_id;//(cach_id instanceof String)?cach_id:ObjectUtils.getListFromNative((NativeArray)cach_id);
                            Object cach_impacts = ((Map) mappin).get("impacts");
                            HttpCache.getInstance().initCache(cache_id, webmethod);
                            if (cach_impacts instanceof Undefined) {
                                LOG.error("Adding listener for webmethod " + webmethod + "... No cache impact specified!");
                            } else if (cach_impacts instanceof Boolean) {
                                Boolean cache_impacts = (Boolean) cach_impacts;
                                context.getAttributes().put("clear_cache", Boolean.FALSE);
                                context.getAttributes().put("cacheable", cache_politics);
                                context.getAttributes().put("impacts", cache_impacts);
                                context.getAttributes().put("cache_id", cache_id);
                                context.getFilters().add(new ServerFilter(requests_queue, (String) handler, webmethod, responsePool));
                            } else {
                                LOG.error("Adding listener for webmethod " + webmethod + "... Wrong impacts datatype");
                            }
                        } else {
                            LOG.error("Adding listener for webmethod " + webmethod + "... Wrong cache_id datatype");
                        }
                    } else {
                        LOG.error("Adding listener for webmethod " + webmethod + "... Wrong cacheable datatype");
                    }
                } else {
                    LOG.error("Adding listener for webmethod " + webmethod + "... Wrong handler datatype");
                }
            }

            contexts.add(context);
        }
        server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());
        server.start();
    }

    public HTTPServer(String event_name, String function_name, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, int port, Scriptable parent, boolean isModern) {
        this.isModern = isModern;
        requests_queue = new ArrayBlockingQueue(1000);
        response_queue = new ArrayBlockingQueue(1000);
        enginePool = new PoolExecutor(3, this, message_queue, commands_queue, events_queue, incoming_events_queue);
        responsePool = new ConcurrentHashMap();
        contexts = new ArrayList();
        this.event_name = event_name;
        this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        this.port = port;

        server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(port), 10);
        } catch (IOException ex) {
            LOG.error("Error starting listener with name: " + name + "... " + ex.getMessage());
        }

        HttpHandler mainhandler = new HttpHandler() {
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                Gson gson = new Gson();
                StringWriter sw = new StringWriter();
                JsonWriter jw = new JsonWriter(sw);
                jw.beginObject();
                gson.toJson(Integer.valueOf(100/*get load factor*/), Integer.class, jw.name("load_factor"));
                jw.name("methods").beginArray();
                for (HttpContext c : contexts) {
                    jw.value(c.getPath().replace("/", ""));
                }
                jw.endArray();
                jw.endObject();
                String response = sw.toString();
                jw.flush();
                sw.flush();
                jw.close();
                sw.close();
                exchange.sendResponseHeaders(200, response.length());
                exchange.getResponseBody().write(response.getBytes("UTF-8"));
                exchange.close();
            }
        };

        main_context = server.createContext("/", mainhandler);
        //main_context.getFilters().add(new RestfulFilter(requests_queue, ""));

        HttpContext context = server.createContext("/" + event_name, this);
        context.getFilters().add(new ServerFilter(requests_queue, function_name, event_name, responsePool));
        contexts.add(context);
        server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());
        server.start();
    }

    @Override
    public void registerMethods(CommonEngine jsEngine) {

    }

    @Override
    public void registerEvents() {
        /*events.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers after user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"ProxyEventPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject passed through filter. Attached object is a HttpObject"));
        events.add(new Event(prefix+"ProxyEventDoesNotPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject is blocked by filter. Attached object is a blocked HttpObject"));*/
    }

    @Override
    public void handle(HttpExchange exc) throws IOException {
        boolean gzip = System.getProperty("thrones.http.gzip").indexOf("true") >= 0 || System.getProperty("thrones.http.gzip").indexOf("yes") >= 0;
        String thread_no = (String) exc.getAttribute("thread_no");
        if (gzip) {
            //exc.getResponseHeaders().set("Content-Encoding", "gzip");

            //exc.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
            OutputStream os = exc.getResponseBody();

            LOG.info("Context processing. Thread#" + thread_no);
            
        } else {
            String path = exc.getHttpContext().getPath();
            LOG.info("Context called: " + path);
            String is_help = (String) exc.getAttribute("help");
            String is_clear = (String) exc.getAttribute("clear");
            exc.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");

            LOG.info("Context processing. Thread#" + thread_no);
            PrintWriter out = new PrintWriter(exc.getResponseBody());
            if (is_help != null) {
                exc.sendResponseHeaders(200, 0);
                String func = (String) exc.getAttribute("function");
                BasicScript bs = LibraryHolder.getInstance().getScript(func);
                String script_body = bs == null ? "" : bs.getSource();
                out.println(script_body);
                exc.setAttribute("help", null);
                out.close();
                exc.close();
            } else if (is_clear != null) {
                exc.sendResponseHeaders(200, 0);
                if (applyCachePolitics(path.replace("/", ""), false)) {
                    out.println("ok");
                } else {
                    out.println("no linked cache");
                }
                exc.setAttribute("clear", null);
                out.close();
                exc.close();
            }
      
        }

    }

    private boolean applyCachePolitics(String func, boolean take_impact_attention) {
        Object cache_id = null;//=(String)exc.getAttribute("cache_id");
        //List<String> cache_id_list=null;//=(String)exc.getAttribute("cache_id");
        Boolean impacts = false;
        Boolean clear_cache = false;
        for (HttpContext c : contexts) {
            if (c.getPath().replace("/", "").equalsIgnoreCase(func)) {
                if (take_impact_attention) {
                    impacts = (Boolean) c.getAttributes().get("impacts");
                    if (impacts != null) {
                        if (impacts) {
                            cache_id = c.getAttributes().get("cache_id");
                        }
                    }
                } else {
                    cache_id = c.getAttributes().get("cache_id");
                }

                Object o = c.getAttributes().get("clear_cache");

                if (o != null) {
                    if (o instanceof Boolean) {
                        if (((Boolean) o)) {
                            c.getAttributes().put("clear_cache", false);
                        }
                    }
                }
                break;
            }
        }
        if (cache_id != null) {
            for (HttpContext c : contexts) {
                Object c_cache_id = c.getAttributes().get("cache_id");
                if (c_cache_id != null) {
                    if (c_cache_id instanceof String) {
                        if (cache_id instanceof String) {
                            if (((String) c_cache_id).equalsIgnoreCase((String) cache_id)) {
                                c.getAttributes().put("clear_cache", true);
                            }
                        } else if (cache_id instanceof List) {
                            for (String c_c_cache_id : (List<String>) cache_id) {
                                if (c_c_cache_id.equalsIgnoreCase((String) c_cache_id)) {
                                    c.getAttributes().put("clear_cache", true);
                                }
                            }
                        }
                    } else if (c_cache_id instanceof List) {
                        if (cache_id instanceof String) {
                            for (String c_c_cache_id : (List<String>) c_cache_id) {
                                if (c_c_cache_id.equalsIgnoreCase((String) cache_id)) {
                                    c.getAttributes().put("clear_cache", true);
                                }
                            }
                        } else if (cache_id instanceof List) {
                            for (String c_c_cache_id : (List<String>) c_cache_id) {
                                for (String c_c_c_cache_id : (List<String>) cache_id) {
                                    if (c_c_cache_id.equalsIgnoreCase(c_c_c_cache_id)) {
                                        c.getAttributes().put("clear_cache", true);
                                    }
                                }
                                /*if(c_c_cache_id.equalsIgnoreCase((String)cache_id))
                                {
                                    c.getAttributes().put("clear_cache", true);
                                }*/
                            }
                        }
                    }

                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void listen() {
        Gson gson = new Gson();
        ThreadsHolder.getInstance().addThread(this);
        for (; !stop;) {
            try {
                LOG.info("wait for task... ");
                HTTPServerTask e = (HTTPServerTask) requests_queue.take();

                LOG.info("new task: " + e + " for thread: " + e.thread_no);
                if (e != null) {
                    LOG.info("new task with function: " + e.function);
                    if (e.function != null) {
                        this.status = "processing_event";
                        String script_body;

                        script_body = e.function + "(" + e.request_body + ")\n";

                        enginePool.runHttpScript(e, script_body, responsePool);

                        this.status = "listening";
                    }
                } else if (e.function == null && e.method == null && e.request_body == null) {
                    break;
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
                ex.printStackTrace();
                break;
            }
        }
        server.stop(0);
        //server.
        ThreadsHolder.getInstance().removeThread(this);
    }

    @Override
    public void stop() {
        LOG.info("Command STOP received for HTTP listener ID: " + id);
        stop = true;
        //server.stop(0);
        HTTPServerTask e = new HTTPServerTask(null, null, null, null, null, null, null);
        requests_queue.add(e);

    }

}
