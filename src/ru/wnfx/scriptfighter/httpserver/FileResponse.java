/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.httpserver;

import ru.wnfx.scriptfighter.file.core.FileInterface;

/**
 *
 * @author sisyandra
 */
public class FileResponse {
    private String filename;
    private String asFile;
    private FileInterface pointer;
    
    public FileResponse() 
    {
        filename = null;
        pointer = null;
        asFile = null;
    };

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the pointer
     */
    public FileInterface getPointer() {
        return pointer;
    }

    /**
     * @param pointer the pointer to set
     */
    public void setPointer(FileInterface pointer) {
        this.pointer = pointer;
    }

    /**
     * @return the asFile
     */
    public String getAsFile() {
        return asFile;
    }

    /**
     * @param asFile the asFile to set
     */
    public void setAsFile(String asFile) {
        this.asFile = asFile;
    }
    
    
}
