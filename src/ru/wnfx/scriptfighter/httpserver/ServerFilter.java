/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.httpserver;

import ru.wnfx.scriptfighter.interfaces.http.*;
import thrones.*;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import ru.wnfx.scriptfighter.utils.StringUtils;
/**
 *
 * @author sergk
 */
public class ServerFilter extends Filter {

    private ArrayBlockingQueue requests_queue;
    private String function_name;
    private String method;
    private ArrayList<Map.Entry<String, List<String>>> headers;
    //private OutputStream outputStream;
    ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool;

    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(ServerFilter.class);
    
    public ServerFilter(ArrayBlockingQueue requests_queue, String function_name, String method, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool)
    {
        this.requests_queue = requests_queue;
        this.function_name=function_name;
        this.method=method;
        this.responsePool = responsePool;
        this.headers = new ArrayList();
        
    }
    
    @Override
    public String description() {
        return "Parses the requested URI for parameters";
    }

    @Override
    public void doFilter(HttpExchange exchange, Chain chain)
        throws IOException 
    {
        //outputStream = exchange.getResponseBody();
                
        parseGetParameters(exchange);
        parsePostParameters(exchange);
        parseOptionsParameters(exchange);
        this.headers = new ArrayList();
        for (Map.Entry<String, List<String>> header : exchange.getRequestHeaders().entrySet()) {
            this.headers.add(header);
        }
        
        chain.doFilter(exchange);
    }    
    
    private String getLocale(HttpExchange exchange)
    {
        String locale = null;
        for (Map.Entry<String, List<String>> header : exchange.getRequestHeaders().entrySet()) {
            if(header.getKey().equalsIgnoreCase("Language"))
            {
                if(header.getValue()!=null)
                {
                    if(header.getValue().size()>0)
                        locale = header.getValue().get(0).toUpperCase();
                }
            }
        }
        return locale;
    }

    private void parseGetParameters(HttpExchange exchange)
        throws UnsupportedEncodingException 
    {
        if ("get".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            Map<String, Object> parameters = new HashMap<String, Object>();
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            
            //need for check for valid JSON

            if(query!=null) 
            {
                if(URLDecoder.decode(query,"UTF-8").equalsIgnoreCase("help"))
                {
                    exchange.setAttribute("help", "true");
                    exchange.setAttribute("function", function_name);
                }
                else if(URLDecoder.decode(query,"UTF-8").equalsIgnoreCase("clear"))
                {
                    exchange.setAttribute("clear", "true");
                    exchange.setAttribute("function", function_name);
                }
                else
                {
                    //String path=exchange.getHttpContext().getPath();
                    String params = URLDecoder.decode(query,"UTF-8");
                    String locale = getLocale(exchange);
                    String paramsLocale = params+(locale==null?"":("&locale="+locale));
                    
                    if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                    {
                        String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                        exchange.setAttribute("thread_no", thread_no); 
                        responsePool.put(thread_no, new ArrayBlockingQueue(10));
                        HTTPServerTask task = new HTTPServerTask(method, "GET", URLDecoder.decode(query,"UTF-8"), StringUtils.queryToJson(params), function_name, thread_no, headers);
                        task.outputStream = exchange;
                        requests_queue.offer(task);
                    }
                }
            }
            else
            {
                String locale = getLocale(exchange);
                String paramsLocale = ""+(locale==null?"":("&locale="+locale));
                //+(locale==null?"":("&locale="+locale))
                //    String paramsLocale = params+(locale==null?"":("&locale="+locale));
                if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                {
                    String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                    exchange.setAttribute("thread_no", thread_no); 
                    responsePool.put(thread_no, new ArrayBlockingQueue(10));
                    HTTPServerTask task = new HTTPServerTask(method, "GET", URLDecoder.decode(query,"UTF-8"), "",function_name, thread_no, headers);
                    task.outputStream = exchange;
                    requests_queue.offer(task);
                }
            }
        }
        //parseQuery(query, parameters);
        //exchange.setAttribute("parameters", parameters);
    }

    private void parsePostParameters(HttpExchange exchange)
        throws IOException 
    {
        
        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            @SuppressWarnings("unchecked")
            Map<String, Object> parameters =
                (Map<String, Object>)exchange.getAttribute("parameters");
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            InputStreamReader isr =
                new InputStreamReader(exchange.getRequestBody(),"utf-8");
            BufferedReader br = new BufferedReader(isr);
            String result="";

            for(String tmp_result=br.readLine();tmp_result!=null;tmp_result=br.readLine())
            {
                result+=tmp_result;
            }

            if(result.equalsIgnoreCase("help"))
            {
                exchange.setAttribute("help", "true");
                exchange.setAttribute("function", function_name);
            }
            else
            {
                //need for check for valid JSON
                String locale = getLocale(exchange);
                String paramsLocale = result+(locale==null?"":("&locale="+locale));
                    
                if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                {
                    String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                    exchange.setAttribute("thread_no", thread_no); 
                    responsePool.put(thread_no, new ArrayBlockingQueue(10));
                    HTTPServerTask task = new HTTPServerTask(method, "POST", URLDecoder.decode(query,"UTF-8"), result, function_name, thread_no, headers);
                    task.outputStream = exchange;
                    requests_queue.offer(task);
                }
            }
        }
    }
    
    private void parseOptionsParameters(HttpExchange exchange)
        throws IOException 
    {
        
        if ("options".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
           exchange.setAttribute("help", "true");
           exchange.setAttribute("function", function_name);
        }
            
    }

     @SuppressWarnings("unchecked")
     private void parseQuery(String query, Map<String, Object> parameters)
         throws UnsupportedEncodingException {

         if (query != null) {
             String pairs[] = query.split("[&]");

             for (String pair : pairs) {
                 String param[] = pair.split("[=]");

                 String key = null;
                 String value = null;
                 if (param.length > 0) {
                     key = URLDecoder.decode(param[0],
                         System.getProperty("file.encoding"));
                 }

                 if (param.length > 1) {
                     value = URLDecoder.decode(param[1],
                         System.getProperty("file.encoding"));
                 }

                 if (parameters.containsKey(key)) {
                     Object obj = parameters.get(key);
                     if(obj instanceof List<?>) {
                         List<String> values = (List<String>)obj;
                         values.add(value);
                     } else if(obj instanceof String) {
                         List<String> values = new ArrayList<String>();
                         values.add((String)obj);
                         values.add(value);
                         parameters.put(key, values);
                     }
                 } else {
                     parameters.put(key, value);
                 }
             }
         }
    }
}
