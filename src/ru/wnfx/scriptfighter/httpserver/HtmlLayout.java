/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.httpserver;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import static ru.wnfx.scriptfighter.db.nosql.NOSQL.LOG;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;

/**
 *
 * @author sisyandra
 */
public class HtmlLayout extends Library {
    private OutputStreamWriter bw;
    
    public HtmlLayout(CommonEngine g) {
        prefix = "HL";
        desc = "inline html API";
        if (g != null) {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
        LOG.info("First init html layout");
    }
    
    @JavaScriptMethod(type = "http", autocomplete = true, params = "", desc = "inits document")
    public HtmlLayout StartDocument() throws IOException
    {
        bw = g.getWriter();
        bw.write("<html>");
        return this;
    }
}
