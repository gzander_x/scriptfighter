/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.docs.core;

import ru.wnfx.scriptfighter.html.core.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;

import com.google.api.client.auth.oauth2.Credential;
//import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
//import com.google.api.client.json.jackson2.JacksonFactory;
//import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sergk
 */
public class GoogleSheets extends Library {

    Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(Html.class);
    private String doc_url;
    File reader;
    String credentials = "{\n" +
"  \"type\": \"service_account\",\n" +
"  \"project_id\": \"parser-1506598911153\",\n" +
"  \"private_key_id\": \"b41994f4b89c563105c096be73ae7fd94e709fa1\",\n" +
"  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC9xOMYM1H1lXUw\\neOisZOUCd5eyvFqdw2SZmUD7M0FHJzomc7HW7hjeJ0TrpAG7iHS1ulAeFit1YAuI\\nvHiX95YH0H3yDaI+aV4AiWydy+oBTOWCIFGnyNRMF/cgo1SD7wL8Qq/DwyL7iO4o\\nIJ1PicMl7MkjPP6Fjqo8foKu44q2jQB1OWhDYTc6m5u7qg5zT+FIIbaZwFVXXwwe\\n48z2j+q/eMCZGHWZ99WQdYDUcc/qZ7+xaqO3hObkXYW3BlUBcAkpQ4rNoHfZWpKV\\nazedV9MTyIUiXtZ/LAROko3/muOvyAtkG9G/xfEzYhSkpiiqbsVj4JTk5SoVvanO\\ns4mi0lehAgMBAAECggEAQHY/Bht4vcl1M2/+6YCkYkApoadlyK104NA0SKBSjk7+\\n68zlHjNm8x+3w6vEguWJb6nISVASlx53DI2pEUYjCqiB/9UtxbGZIumLodaAuMxc\\nMeW0lyAbLXX+lnGk5eQsyXAFsKP6oCMVc0AQ79HGHn3enT+83bVmkoIO9kWI+xU4\\ngSwPCkL4y8nf8XNBmDiqGZCumbTdkVsMXIaGBb8c5UX6WDBz29Ed4WsGAwjBj8FQ\\nsL+w9sPJzlvS6rT43IwB/NOioQ9t4e1vU1zwGoRBeKu760GKavxyRqc+XLz4GTi8\\nfO+jDNpXqqJhRcgxOOkJmI1uIBOg4d0QQ1c9hggi2QKBgQDrOry1i7bBCw/eu6Wr\\ntqw6d0l8V7ob7A8rOj4fVU0LRl4XZthWWIiRzQgcNzq44p5Ef5sq0N5dqmdu7mPt\\nLVF4qifOgd3ElA7xDIcSbx8w2vWLkqL3d1guhKlBnGfaw+l0cAHPuPAKdtAn6sNq\\nFU5UM5Y3OXDCdugHNojjluZMRwKBgQDOhomqwZQGkEtSVtjQWvXSoD02qmYvC07N\\nAQpiQnhQmZGF9czlu4BPcM4cEzyCxilKC5rgkxBqhXKc7ggiZpDir0uUUON8W1vy\\nHQvweTdebwqgD8ZLHnitHTf3ORifYKXgvM+bWrI/RVfyGyGFDL2cP0YgVmhpTiHa\\nEBx4raF41wKBgQCjbvX6NQ5O5b6rBDlBKvRS0tpt2A3opISNhjPvVTboZIWvQLeC\\nlpScqJW/9OeJ2XoZIYfPobs93M1sl5k6CsiWS7LImhZZJH2VIR0fw7EqIpeD1+f+\\nhB7ygx2OBntp1cB4M/IuPtIYkyBg+422QQNhIU53JCOiGTdw+4T0i72KtQKBgB3s\\nsJXlZqVs0v0jSFcoVuYmAbiiCBxY7xoVRIPoTbagCc33R7Eh8f2QwsyenO31+JWa\\nCN7xcDd/DpReiR8y7VtrOvBKqCuksTXrr4cwiS1NuaRnpLvNtn6e9TUFOOQfBOoD\\ncbF4+srgKVYTSFAXAIJbSLX2zzoy8ATAwN9biUIRAoGBALa8Uk3umbQHy5ss7YF3\\nN1Yaeu8DKc7XiXSqkEtrwDTft9Rmcrl0VsoLLRPLLJPkBmJFVb7XRBO1swBjdl3W\\nJFU91QS4EqgDwVi+sB0JaF6GLx5nODPfk6ZdIqs2AjEcsx0yYRhOtDodPXmoOYHg\\nbLIk+TJp4Mr6+vywh0nAC+/v\\n-----END PRIVATE KEY-----\\n\",\n" +
"  \"client_email\": \"parser@parser-1506598911153.iam.gserviceaccount.com\",\n" +
"  \"client_id\": \"111950553573623587460\",\n" +
"  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
"  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
"  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
"  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/parser%40parser-1506598911153.iam.gserviceaccount.com\"\n" +
"}";
//    private final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);

    public GoogleSheets(CommonEngine g) {
        doc = null;
        elements = null;
        prefix = "GoogleSheets";
        desc = "Google sheet API";
        doc_url = null;

        if (g != null) {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    

    public JacksonFactory jacksonFactory() {
        return new JacksonFactory();
    }

    private Set<String> googleOAuth2Scopes() {
        Set<String> googleOAuth2Scopes = new HashSet();
        googleOAuth2Scopes.add(SheetsScopes.SPREADSHEETS);
        return Collections.unmodifiableSet(googleOAuth2Scopes);
    }

    public GoogleCredential googleCredential() throws IOException {
        InputStream in = new ByteArrayInputStream( credentials.getBytes( "UTF8" ) );
        return GoogleCredential.fromStream(in)
                .createScoped(googleOAuth2Scopes());
    }

    public Sheets googleSheets() throws IOException, GeneralSecurityException {
        return new Sheets(netHttpTransport(), jacksonFactory(), googleCredential());
    }
    
    public NetHttpTransport netHttpTransport() throws GeneralSecurityException, IOException {
        return GoogleNetHttpTransport.newTrustedTransport();
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String url", desc = "open csv doc")
    public GoogleSheets init() throws IOException, GeneralSecurityException {
        
        Sheets sh = googleSheets();
        ValueRange response = sh.spreadsheets().values()
                .get("1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms", "Class Data!A2:E")
                .execute();
        return this;
    }

    

}
