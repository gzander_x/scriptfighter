/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

/**
 *
 * @author sergk
 */
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
//import java.util.concurrent.ConcurrentLinkedQueue;
//import java.util.concurrent.ArrayBlockingQueue;
import com.google.gson.*;
import org.apache.log4j.Logger;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.ArrayList;
import java.util.logging.Level;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.EnginePool;
import ru.wnfx.scriptfighter.engine.core.manager.ScriptTask;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;

/**
 *
 * @author sergk
 */
public class ScriptExecutor extends Scriptable {

    public static final Logger LOG = Logger.getLogger(ScriptExecutor.class);

    public int mode;
    private Gson gson = new Gson();

    private ArrayList<Connectorparam> overriding_params;

    public ScriptExecutor(int mode, ArrayBlockingQueue events_queue) {
        this.message_queue = new LimitedMessageQueue(640);
        this.commands_queue = new ArrayBlockingQueue(1000);
        this.events_queue = events_queue;
        this.incoming_events_queue = new LimitedMessageQueue(640);
        this.mode = mode;
        id = java.util.UUID.randomUUID().toString().replace("-", "");
        overriding_params = null;
        jsEngine = EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
    }

    public ScriptExecutor() {
        this.message_queue = new LimitedMessageQueue(640);
        this.commands_queue = new ArrayBlockingQueue(1000);
        this.events_queue = new ArrayBlockingQueue(10000);
        this.incoming_events_queue = new LimitedMessageQueue(640);
        id = java.util.UUID.randomUUID().toString().replace("-", "");
        jsEngine = EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
    }

    public ScriptExecutor(String scriptBody, int mode, ArrayBlockingQueue events_queue, LimitedMessageQueue message_queue) {
        this.message_queue = message_queue;
        this.commands_queue = new ArrayBlockingQueue(1000);
        this.events_queue = events_queue;
        this.incoming_events_queue = new LimitedMessageQueue(640);
        this.mode = mode;
        id = java.util.UUID.randomUUID().toString().replace("-", "");
        overriding_params = null;
        jsEngine = EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
    }

    public String runScript(String scriptBody) {
        if (overriding_params != null) {
            jsEngine.commonParams = overriding_params;
        }
        try {
            // Read a message sent by client application
            //ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

            Object o = jsEngine.runScript(scriptBody, mode, id);
            if (o instanceof String) {
                return (String) o;
            } else {
                return gson.toJson(o);
            }
        } catch (ScriptStopException ex) {
            java.util.logging.Logger.getLogger(ScriptTask.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            //responsePool.get(identifier).offer(new HTTPResponseTask(method,ex.getMessage(),function, engine.httpCode));
        } catch (Exception e) {
            jsEngine.error("Ошибка на верхнем уровне потока скрипта: " + e.getMessage());
            e.printStackTrace();
            System.out.println("Ошибка на верхнем уровне потока скрипта: " + e.getMessage());
        } catch (ScriptTimestampTooOldException ex) {
            //re-init js engine
            jsEngine = EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
            runScript(scriptBody);
        } finally {

        }
        return null;
    }

    @Override
    public void registerMethods(CommonEngine jsEngine) {

    }

    @Override
    public void registerEvents() {

    }
}
