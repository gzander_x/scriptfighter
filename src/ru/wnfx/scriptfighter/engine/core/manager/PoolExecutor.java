/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

import com.sun.net.httpserver.HttpExchange;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.httpserver.HTTPServerTask;
import ru.wnfx.scriptfighter.interfaces.http.HTTPMethodTask;
import ru.wnfx.scriptfighter.interfaces.http.HTTPResponseTask;

/**
 *
 * @author sisyandra
 */
public class PoolExecutor {

    private ArrayList<CommonEngine> enginePool;
    private final int thread_count;
    Scriptable scr;
    LimitedMessageQueue message_queue;
    ArrayBlockingQueue commands_queue;
    ArrayBlockingQueue events_queue;
    LimitedMessageQueue incoming_events_queue;
    private ScheduledExecutorService pool;
    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PoolExecutor.class);
    
    class ScriptThreadFactory implements ThreadFactory {
        private int counter = 0;
        private String prefix = "Rest thread ";

        public ScriptThreadFactory(String prefix) {
          this.prefix = prefix;
        }

        public Thread newThread(Runnable r) {
          return new Thread(r, prefix + "#" + counter++);
        }
     }

    public PoolExecutor(int tc, Scriptable scr, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue) {
        thread_count = tc;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        enginePool = new ArrayList();
        pool = Executors.newScheduledThreadPool(thread_count * 10, new ScriptThreadFactory("Script thread "));
        ((ScheduledThreadPoolExecutor) pool).setRemoveOnCancelPolicy(true);

        for (int i = 0; i < thread_count; i++) {
            enginePool.add(EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, scr));
        }
    }

    public int runScript(HTTPMethodTask e, String script_body, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool) {
        return runScript(e.method, e.function, e.httpMethod, e.url, e.request_body, script_body, e.thread_no, responsePool, e.headers, e.outputStream);
    }
    
    public int runHttpScript(HTTPServerTask e, String script_body, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool) {
        return runHttpScript(e.method, e.function, e.httpMethod, e.url, e.request_body, script_body, e.thread_no, responsePool, e.headers, e.outputStream);
    }

    public int runScript(String method, String function, String httpMethod, String url, Object pars, String script_body, String identifier, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool, ArrayList<Map.Entry<String, List<String>>> headers, HttpExchange os) {
        long scriptTimestamp = LibraryHolder.getInstance().scriptTimestamp;
        int busy_count = 0;
        for (int i = 0; true; i++) {
            if (i >= thread_count) {
                i = 0;

                if (busy_count >= thread_count) {
                    for (int j = 0; j < thread_count; j++) {
                        long currdate = (new Date()).getTime();
                        //System.out.println("Running "+identifier+" for: "+(currdate-enginePool.get(j).execTime));
                        if ((currdate - enginePool.get(j).execTime) < 300000) {
                            LOG.info("waiting " + identifier + " of engine " + enginePool.get(j).id + " that works " + (currdate - enginePool.get(j).execTime) + "ms");
                        } else {
                            LOG.info("Killing " + identifier + " of engine " + enginePool.get(j).id + " that works " + (currdate - enginePool.get(j).execTime) + "ms");
                            //enginePool.get(j).httpCode = 408;
                            enginePool.get(j).timer.cancel(true);
                            enginePool.get(j).busy = false;
                            enginePool.get(j).execTime = currdate;
                            enginePool.add(j, EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, scr));
                            //CommonEngine ce = enginePool.get(j);
                            //ce.headers = headers;
                            //ScriptPoolTask st = new ScriptPoolTask(ce, method, function, pars, script_body, identifier, responsePool, os);
                            //enginePool.get(j).timer = pool.schedule(st, 1, TimeUnit.MILLISECONDS);
                            busy_count--;
                            return j;
                        }
                    }
                }

                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PoolExecutor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            CommonEngine ce = enginePool.get(i);

            if (!ce.busy) {
                LOG.info("Vacant engine " + ce.id + " found ");
                if (ce.scriptTimestamp < scriptTimestamp) {
                    LOG.info("Too old scripts, restart engine");
                    //enginePool.add(i, EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, scr));
                    LibraryHolder.getInstance().registerLibrary(ce, "");
                    ce.scriptTimestamp = (new Date()).getTime();
                }
                ce.busy = true;
                ce.url = url;
                ce.httpMethod = httpMethod;
                ce.headers = headers;
                LOG.info("Make a task of engine " + ce.id + " for method " + method);
                ScriptPoolTask st = new ScriptPoolTask(ce, method, function, pars, script_body, identifier, responsePool, os);
                ce.timer = pool.schedule(st, 1, TimeUnit.MILLISECONDS);
                return i;
            } else {
                busy_count++;
            }

        }
    }
    
    public int runHttpScript(String method, String function, String httpMethod, String url, String pars, String script_body, String identifier, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool, ArrayList<Map.Entry<String, List<String>>> headers, HttpExchange os) {
        long scriptTimestamp = LibraryHolder.getInstance().scriptTimestamp;
        int busy_count = 0;
        for (int i = 0; true; i++) {
            if (i >= thread_count) {
                i = 0;

                if (busy_count >= thread_count) {
                    for (int j = 0; j < thread_count; j++) {
                        long currdate = (new Date()).getTime();
                        //System.out.println("Running "+identifier+" for: "+(currdate-enginePool.get(j).execTime));
                        if ((currdate - enginePool.get(j).execTime) < 300000) {
                            LOG.info("waiting " + identifier + " of engine " + enginePool.get(j).id + " that works " + (currdate - enginePool.get(j).execTime) + "ms");
                        } else {
                            LOG.info("Killing " + identifier + " of engine " + enginePool.get(j).id + " that works " + (currdate - enginePool.get(j).execTime) + "ms");
                            //enginePool.get(j).httpCode = 408;
                            enginePool.get(j).timer.cancel(true);
                            enginePool.get(j).busy = false;
                            enginePool.get(j).execTime = currdate;
                            enginePool.add(j, EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, scr));
                            //CommonEngine ce = enginePool.get(j);
                            //ce.headers = headers;
                            //ScriptPoolTask st = new ScriptPoolTask(ce, method, function, pars, script_body, identifier, responsePool, os);
                            //enginePool.get(j).timer = pool.schedule(st, 1, TimeUnit.MILLISECONDS);
                            busy_count--;
                            return j;
                        }
                    }
                }

                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PoolExecutor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            CommonEngine ce = enginePool.get(i);

            if (!ce.busy) {
                LOG.info("Vacant engine " + ce.id + " found ");
                if (ce.scriptTimestamp < scriptTimestamp) {
                    LOG.info("Too old scripts, restart engine");
                    //enginePool.add(i, EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, scr));
                    LibraryHolder.getInstance().registerLibrary(ce, "");
                    ce.scriptTimestamp = (new Date()).getTime();
                }
                ce.busy = true;
                ce.url = url;
                ce.httpMethod = httpMethod;
                ce.headers = headers;
                LOG.info("Make a task of engine " + ce.id + " for method " + method);
                HttpScriptPoolTask st = new HttpScriptPoolTask(ce, method, function, pars, script_body, identifier, responsePool, os);
                ce.timer = pool.schedule(st, 1, TimeUnit.MILLISECONDS);
                return i;
            } else {
                busy_count++;
            }

        }
    }

}
