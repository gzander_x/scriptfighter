/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

import com.google.gson.stream.JsonWriter;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import ru.wnfx.scriptfighter.engine.core.ScriptStopException;
import ru.wnfx.scriptfighter.engine.core.ScriptTimestampTooOldException;
import static ru.wnfx.scriptfighter.engine.core.manager.HttpScriptPoolTask.LOG;
import ru.wnfx.scriptfighter.httpserver.FileResponse;
import ru.wnfx.scriptfighter.httpserver.HtmlLayout;
import ru.wnfx.scriptfighter.interfaces.http.HTTPResponseTask;
import ru.wnfx.scriptfighter.interfaces.http.HttpCache;
import ru.wnfx.scriptfighter.utils.ObjectUtils;

/**
 *
 * @author sisyandra
 */
public class ScriptPoolTask implements Runnable {
    private static final Map<String,String> MIME_MAP = new HashMap();
    static {
        MIME_MAP.put("appcache", "text/cache-manifest");
        MIME_MAP.put("css", "text/css");
        MIME_MAP.put("gif", "image/gif");
        MIME_MAP.put("html", "text/html");
        MIME_MAP.put("js", "application/javascript");
        MIME_MAP.put("json", "application/json");
        MIME_MAP.put("jpg", "image/jpeg");
        MIME_MAP.put("jpeg", "image/jpeg");
        MIME_MAP.put("mp4", "video/mp4");
        MIME_MAP.put("pdf", "application/pdf");
        MIME_MAP.put("png", "image/png");
        MIME_MAP.put("svg", "image/svg+xml");
        MIME_MAP.put("xlsm", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        MIME_MAP.put("xml", "application/xml");
        MIME_MAP.put("zip", "application/zip");
        MIME_MAP.put("md", "text/plain");
        MIME_MAP.put("txt", "text/plain");
        MIME_MAP.put("php", "text/plain");
    };
    
    private static String lookupMime(String path) {
        String ext = getExt(path).toLowerCase();
        return MIME_MAP.getOrDefault(ext, "application/octet-stream");
    }
    
    private static String getExt(String path) {
        int slashIndex = path.lastIndexOf('/');
        String basename = (slashIndex < 0) ? path : path.substring(slashIndex + 1);

        int dotIndex = basename.lastIndexOf('.');
        if (dotIndex >= 0) {
            return basename.substring(dotIndex + 1);
        } else {
            return "";
        }
    }
    
    CommonEngine engine;
    String method;
    String function;
    Object pars;
    String script_body;
    String identifier;
    int httpCode;
    HttpExchange os;
    boolean gzip = System.getProperty("thrones.http.gzip").indexOf("true") >= 0 || System.getProperty("thrones.http.gzip").indexOf("yes") >= 0;

    ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool;
    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ScriptPoolTask.class);

    public ScriptPoolTask(CommonEngine engine, String method, String function, Object pars, String script_body, String identifier, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool, HttpExchange os) {

        this.engine = engine;
        this.method = method;
        this.function = function;
        this.script_body = script_body;
        this.identifier = identifier;
        this.responsePool = responsePool;
        this.pars = pars;
        this.os = os;
    }

    private String getLocale(HttpExchange exchange) {
        String locale = null;
        for (Map.Entry<String, List<String>> header : exchange.getRequestHeaders().entrySet()) {
            if (header.getKey().equalsIgnoreCase("Language")) {
                if (header.getValue() != null) {
                    if (header.getValue().size() > 0) {
                        locale = header.getValue().get(0).toUpperCase();
                    }
                }
            }
        }
        return locale;
    }

    @Override
    public void run() {
        OutputStreamWriter osw = null;
        String old_name = Thread.currentThread().getName();
        try {
            Thread.currentThread().setName(old_name+". Executing method "+function);
            LOG.info("Task execution started for: " + method);
            Object o = engine.runScriptWithParam(function, pars, 0, identifier);//engine.runScript(script_body, 0, identifier);
            if (o instanceof Throwable) {
                httpCode = 500;
            } else {
                httpCode = engine.httpCode;
            }
            try {
                if (gzip && !(o instanceof FileResponse)) {
                    os.getResponseHeaders().set("Content-Encoding", "gzip");
                }
                
                for(Map<String, String> he: engine.response_headers){
                    for(Entry<String, String> en: he.entrySet())
                    {
                        os.getResponseHeaders().set(en.getKey(), en.getValue());
                    }
                }


                LOG.info("start writing body for method " + method);
                if (gzip && !(o instanceof FileResponse)) {
                    if(o==null)
                        os.sendResponseHeaders(404, 0);
                    else
                        os.sendResponseHeaders(httpCode, 0);
                    GZIPOutputStream gos = new GZIPOutputStream(os.getResponseBody());
                    osw = new OutputStreamWriter(gos);
                } else {
                    osw = new OutputStreamWriter(os.getResponseBody());
                }
                
                engine.setWriter(osw);

                if(o instanceof HtmlLayout)
                {
                    if(!gzip)
                        os.sendResponseHeaders(httpCode, 0);
                    StringWriter sw = new StringWriter();
                    sw.append(((HtmlLayout)o).toString());
                }
                else if (o instanceof FileResponse) {
                    httpCode = 200;
                    LOG.info("File response found");
                    FileResponse fr = (FileResponse)o;
                    if(!fr.getPointer().checkFileExists(fr.getFilename()))
                    {
                        os.sendResponseHeaders(404, 0);
                    }
                    else
                    {
                        String mimeType = lookupMime((fr.getAsFile()!=null?fr.getAsFile():fr.getFilename()));
                        if(mimeType.contains("application")||mimeType.contains("text"))
                            os.getResponseHeaders().add("Content-Disposition", "attachment; filename=" + URLEncoder.encode((fr.getAsFile()!=null?fr.getAsFile():fr.getFilename())));
                        os.getResponseHeaders().set("Content-Type", mimeType);
                        /*for(Map<String, String> he: engine.response_headers){
                            for(Entry<String, String> en: he.entrySet())
                            {
                                os.getResponseHeaders().set(en.getKey(), en.getValue());
                            }
                        }*/
                        os.sendResponseHeaders(httpCode, 0);

                        //os.getResponseHeaders().add("Content-Disposition", "attachment; filename=" + fr.getFilename());
                        fr.getPointer().readFile(fr.getFilename(), os.getResponseBody());
                    }
                }
                else if(o==null)
                {
                    if(!gzip)
                        os.sendResponseHeaders(404, 0);
                }
                else
                {
                    if(!gzip)
                        os.sendResponseHeaders(httpCode, 0);
                    JsonWriter jww = new JsonWriter(osw);
                    StringWriter sw = null;
                    JsonWriter cache_writer = null;

                    if (HttpCache.getInstance().existsCache(method) && httpCode == 200) {
                        sw = new StringWriter();
                        cache_writer = new JsonWriter(sw);
                    }
                    ObjectUtils.streamSerializer(jww, cache_writer, ObjectUtils.convert(o), null);

                    if (cache_writer != null && httpCode == 200) {
                        String locale = getLocale(os);
                        String paramsLocale = pars + (locale == null ? "" : ("&locale=" + locale));
                        HttpCache.getInstance().putCache(method, paramsLocale, sw.toString());
                        sw.flush();
                        sw.close();
                    }
                    LOG.info("end writing body for method " + method);
                }
                LOG.info("end writing body for method " + method);
                osw.flush();
                osw.close();
            } catch (IOException ee) {
                ee.printStackTrace();
            }
        } catch (ScriptStopException ex) {
            Logger.getLogger(ScriptTask.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (osw != null) {
                try {
                    osw.append("{\"desc\":\"Script execution terminated\"}");
                } catch (IOException ex1) {
                    Logger.getLogger(ScriptPoolTask.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } catch (ScriptTimestampTooOldException ex) {
            Logger.getLogger(ScriptPoolTask.class.getName()).log(Level.SEVERE, null, ex);
            if (osw != null) {
                try {
                    osw.append("{\"desc\":\"Script body was changed\"}");
                } catch (IOException ex1) {
                    Logger.getLogger(ScriptPoolTask.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (osw != null) {
                try {
                    osw.append("{\"desc\":\"" + ex.getMessage() + "\"}");
                } catch (IOException ex1) {
                    Logger.getLogger(ScriptPoolTask.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }
        finally
        {
            Thread.currentThread().setName(old_name);
        }
    }
}
