/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

/**
 *
 * @author sergk
 */
import com.google.gson.Gson;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import static ru.wnfx.scriptfighter.engine.core.manager.NashornEngine.LOG;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;


public class SunRhinoEngine extends CommonEngine
{
    private ScriptEngine jsEngine;
    private Gson gson;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(SunRhinoEngine.class);
    
    public SunRhinoEngine(LimitedMessageQueue q, ArrayBlockingQueue bq, ArrayBlockingQueue eq, LimitedMessageQueue incoming_events_queue, Scriptable owner)
    {
        super(q,  bq,  eq,  incoming_events_queue, owner);
        //this.g =g;
        ScriptEngineManager scriptEngineMgr = new ScriptEngineManager();
        jsEngine = scriptEngineMgr.getEngineByName("rhino");

        if (jsEngine == null) {
            LOG.error("No script engine found for JavaScript");
            System.exit(1);
        }
        gson = new Gson();
        StringWriter scriptOutput = new StringWriter();
            jsEngine.getContext().setWriter(new PrintWriter(scriptOutput));
    }
    
    @Override
    public Object eval(String scriptBody) throws ScriptException
    {
        return jsEngine.eval(scriptBody);
    }
    
    @Override
    public void put(String name, Object obj)
    {
        jsEngine.put(name, obj);
    }
    
    @Override
    public Object run(String method, String param) throws ScriptException
    {
        Map<String, Object> mapParam = gson.fromJson(param, Map.class);
        Invocable invocable = (Invocable) jsEngine;
        Object result = null;
        try {
            result = invocable.invokeFunction(method, mapParam);
        } catch (NoSuchMethodException ex) {
            LOG.error("No method found");
        }
        return result;
    }
    
    @Override
    public Object runWithObject(String method, Map<String, Object> param) throws ScriptException
    {
        //Map<String, Object> mapParam = gson.fromJson(param, Map.class);
        Invocable invocable = (Invocable) jsEngine;
        Object result = null;
        try {
            result = invocable.invokeFunction(method, param);
        } catch (NoSuchMethodException ex) {
            LOG.error("No method found");
        }
        return result;
    }
    
    @Override
    public Object get(String name)
    {
        return jsEngine.get(name);
    }
    
    @Override
    public ScriptContext getContext()
    {
        return jsEngine.getContext();
    }
    
    @Override
    public Bindings getBindings()
    {
        return jsEngine.getBindings(ScriptContext.ENGINE_SCOPE);
    }

    @Override
    public void setBindings(Bindings b) {
        jsEngine.setBindings(b, ScriptContext.GLOBAL_SCOPE);
    }
}

