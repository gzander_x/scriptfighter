/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core.manager;

/**
 *
 * @author sergk
 */
import com.google.gson.Gson;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.interfaces.http.HttpSnapshot;
import ru.wnfx.scriptfighter.utils.ObjectUtils;


public class NashornEngine extends CommonEngine
{
    private ScriptEngine jsEngine;
    ScriptContext scriptCtxt;
    private Gson gson;
    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(NashornEngine.class);
    boolean need_snaphot = System.getProperty("thrones.http.snapshots").contains("true") || System.getProperty("thrones.http.snapshots").indexOf("yes") >= 0;
    
    public NashornEngine(LimitedMessageQueue q, ArrayBlockingQueue bq, ArrayBlockingQueue eq, LimitedMessageQueue incoming_events_queue, Scriptable owner)
    {
        super(q,  bq,  eq,  incoming_events_queue, owner);
        //this.g =g;
        //ScriptEngine engine = factory.getEngineByName("nashorn");
        ScriptEngineManager scriptEngineMgr = new ScriptEngineManager();
        jsEngine = scriptEngineMgr.getEngineByName("nashorn");
        
        scriptCtxt = jsEngine.getContext();

        if (jsEngine == null) {
            LOG.error("No script engine found for JavaScript");
            System.exit(1);
        }
        
        StringWriter scriptOutput = new StringWriter();
        gson = new Gson();
        scriptCtxt.setWriter(new PrintWriter(scriptOutput));
    }
    
    @Override
    public Object eval(String scriptBody) throws ScriptException
    {
        return jsEngine.eval(scriptBody);
    }
    
    @Override
    public Object run(String method, String param) throws ScriptException
    {
        Map<String, Object> mapParam = null;
        
        try
        {
            mapParam = gson.fromJson(param, Map.class);
        }
        catch(Exception ex)
        {
            LOG.error("Some kind of garbage as input param: "+param+". Exception: "+ex.getMessage());
        }
        
        Invocable invocable = (Invocable) jsEngine;
        Object result = null;
        try {
            result = invocable.invokeFunction(method, mapParam);
            
            if(this.need_snaphot)
            {
                Object tmp = ObjectUtils.convert(result);
                HttpSnapshot.getInstance().takeSnapshot(method, param, tmp, httpCode, headers, response_headers);
            }
        } catch (NoSuchMethodException ex) {
            LOG.error("No method found");
        }
        return result;
    }
    
    @Override
    public Object runWithObject(String method, Map<String, Object> param) throws ScriptException
    {
        Invocable invocable = (Invocable) jsEngine;
        Object result = null;
        try {
            result = invocable.invokeFunction(method, param);
            if(result instanceof Map)
                HttpSnapshot.getInstance().takeSnapshot(method, param, result, httpCode, headers, response_headers);
        } catch (NoSuchMethodException ex) {
            LOG.error("No method found");
        }
        return result;
    }
    
    @Override
    public void put(String name, Object obj)
    {
        jsEngine.put(name, obj);
    }
    
    @Override
    public Object get(String name)
    {
        return jsEngine.get(name);
    }
    
    @Override
    public ScriptContext getContext()
    {
        return jsEngine.getContext();
    }
    
    @Override
    public Bindings getBindings()
    {
        return jsEngine.getBindings(ScriptContext.ENGINE_SCOPE);
    }

    @Override
    public void setBindings(Bindings b) {
        jsEngine.setBindings(b, ScriptContext.GLOBAL_SCOPE);
    }
}

