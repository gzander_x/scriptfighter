/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.engine.core.manager;

import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.wnfx.scriptfighter.engine.core.ScriptStopException;
import ru.wnfx.scriptfighter.engine.core.ScriptTimestampTooOldException;
import ru.wnfx.scriptfighter.interfaces.http.HTTPResponseTask;

/**
 *
 * @author sisyandra
 */
public class ScriptTask extends TimerTask {
    CommonEngine engine;
    String method; 
    String function;
    String script_body;
    String identifier;
    ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool;
    
    public ScriptTask(CommonEngine engine, String method, String function, String script_body, String identifier, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool){
        
        this.engine=engine; 
        this.method=method;
        this.function=function;
        this.script_body=script_body;
        this.identifier=identifier;
        this.responsePool=responsePool;
    }
    
    @Override
    public void run() {
        try
        {
            Object o = engine.runScript(script_body, 0, identifier);
            HTTPResponseTask task = null;
            if(o instanceof Throwable)
                task = new HTTPResponseTask(method,((Throwable)o).getMessage(),function,500);
            else
                task = new HTTPResponseTask(method,o.toString(),function);
            responsePool.get(identifier).offer(task);
        }
        catch (ScriptStopException ex) 
        {
            Logger.getLogger(ScriptTask.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            responsePool.get(identifier).offer(new HTTPResponseTask(method,ex.getMessage(),function, engine.httpCode));
        }
        catch (ScriptTimestampTooOldException ex) 
        {
            Logger.getLogger(ScriptTask.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            responsePool.get(identifier).offer(new HTTPResponseTask(method,ex.getMessage(),function, 500));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            responsePool.get(identifier).offer(new HTTPResponseTask(method,ex.getMessage(),function, 500));
        } 
    }
}
