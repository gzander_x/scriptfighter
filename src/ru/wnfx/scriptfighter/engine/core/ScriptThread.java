/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.engine.core;

/**
 *
 * @author sergk
 */
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
import org.apache.log4j.Logger;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.ArrayList;
import java.util.logging.Level;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.EnginePool;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;


/**
 *
 * @author sergk
 */
public class ScriptThread extends Scriptable implements Runnable 
{
    public static final Logger LOG=Logger.getLogger(ScriptThread.class);

    private String scriptBody;
    public int mode;
    
    private ArrayList<Connectorparam> overriding_params;

     public ScriptThread(String scriptBody, int mode, ArrayBlockingQueue events_queue) 
     {
         Thread t = new Thread(this);
         
         this.message_queue = new LimitedMessageQueue(640);
         this.commands_queue = new ArrayBlockingQueue(1000);
         this.events_queue = events_queue; 
         this.incoming_events_queue = new LimitedMessageQueue(640); 
         this.scriptBody=scriptBody;
         this.mode = mode;
         id=java.util.UUID.randomUUID().toString().replace("-", "");
         overriding_params=null;
         t.start();
     }
     
     public ScriptThread(String scriptBody, Scriptable owner) 
     {
         Thread t = new Thread(this);
         this.message_queue = owner.message_queue;
         this.commands_queue = new ArrayBlockingQueue(1000);
         this.events_queue = owner.events_queue; 
         this.incoming_events_queue = new LimitedMessageQueue(640); 
         this.scriptBody=scriptBody;
         this.mode = owner.mode;
         id=java.util.UUID.randomUUID().toString().replace("-", "");
         if(jsEngine!=null)
         {
            overriding_params=jsEngine.commonParams;
         }
         t.start();
     }
     
     public ScriptThread(String scriptBody, int mode, ArrayBlockingQueue events_queue, LimitedMessageQueue message_queue) 
     {
         Thread t = new Thread(this);
         
         this.message_queue = message_queue;
         this.commands_queue = new ArrayBlockingQueue(1000);
         this.events_queue = events_queue; 
         this.incoming_events_queue = new LimitedMessageQueue(640); 
         this.scriptBody=scriptBody;
         this.mode = mode;
         id=java.util.UUID.randomUUID().toString().replace("-", "");
         overriding_params=null;
         t.start();
     }

    @Override
     public void run() 
     {
         this.status="starting";
         ThreadsHolder.getInstance().addThread(this);
         jsEngine=EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue,  this);
         //g=new Engine(message_queue, commands_queue, events_queue, incoming_events_queue, this, jsEngine);
         
         if(overriding_params!=null)
         {
             jsEngine.commonParams=overriding_params;
         }
         try 
         {
         // Read a message sent by client application
             //ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            
            jsEngine.runScript(scriptBody, mode, id);
         } 
         catch (ScriptStopException ex) 
         {
            ex.printStackTrace();
         }
         catch (ScriptTimestampTooOldException ex) 
         { 
            //re-init js engine
            jsEngine=EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue,  this);
             try 
             {
                 jsEngine.runScript(scriptBody, mode, id);
             } 
             catch (ScriptStopException exxx) 
             {
               exxx.printStackTrace();
             }
             catch (ScriptTimestampTooOldException ex1) 
             {
                 java.util.logging.Logger.getLogger(ScriptThread.class.getName()).log(Level.SEVERE, null, ex1);
             }
         } 
         catch (Exception e) 
         {
             jsEngine.error("Ошибка на верхнем уровне потока скрипта: "+e.getMessage());
         } 
         this.status="waiting_messages";
         ThreadsHolder.getInstance().removeThread(this);
     }
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {
    
    }
    
    @Override
    public void registerEvents()
    {
    
    }
}