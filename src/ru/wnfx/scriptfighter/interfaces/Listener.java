/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces;

/**
 *
 * @author sergk
 */
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import org.apache.log4j.Logger;
import java.util.concurrent.ArrayBlockingQueue;


/**
 *
 * @author sergk
 */
public abstract class Listener extends Scriptable implements Runnable
{
     private static final Logger LOG=Logger.getLogger(Listener.class);

     public String event_name;
     public String function_name;
     protected volatile boolean stop;
     //protected volatile String state;
     //public 
     //ArrayList<ScriptThread> threads;
     
     public Listener() 
     {
         Thread t = new Thread(this);
         t.start();
         this.events_queue = new ArrayBlockingQueue(10000);
         event_name="";
         id=java.util.UUID.randomUUID().toString().replace("-", "");
         stop=false;
         //this.threads=threads;
     }
     
     public abstract void listen();
     
     public abstract void stop();

    @Override
     public void run() 
     {
         this.status="listening";
         try 
         {
             listen();
         } 
         catch (Exception e) 
         {
             LOG.error(e.getMessage());
             //g.error("Ошибка на верхнем уровне потока скрипта: "+e.getMessage());
         } 
         this.status="idle";
     }
}