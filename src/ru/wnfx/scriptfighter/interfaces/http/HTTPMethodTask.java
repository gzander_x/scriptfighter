/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import com.sun.net.httpserver.HttpExchange;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sergk
 */
public class HTTPMethodTask {

    public String function;
    public String method;
    public Object request_body;
    public String thread_no;
    public String httpMethod;
    public String url;
    public HttpExchange outputStream;
    public ArrayList<Map.Entry<String, List<String>>> headers;

    public HTTPMethodTask(String method, String httpMethod, String url, Object task, String function, String thread_no, ArrayList<Map.Entry<String, List<String>>> headers) {
        this.function = function;
        request_body = task;
        this.method = method;
        this.thread_no = thread_no;
        this.headers = headers;
        this.httpMethod = httpMethod;
        this.url = url;
    }

    public ArrayList<Map.Entry<String, List<String>>> getHeaders() {
        return headers;
    }

    public void addHeader(Map.Entry<String, List<String>> h) {
        headers.add(h);
    }
}
