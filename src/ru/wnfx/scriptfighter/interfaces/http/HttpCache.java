/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPOutputStream;
import ru.wnfx.scriptfighter.engine.core.Webmethod;

/**
 *
 * @author sisyandra
 */
public class HttpCache {

    private static volatile HttpCache instance;
    private volatile ConcurrentHashMap<String, Webmethod> cacheMap;
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(HttpCache.class);
    boolean gzip = System.getProperty("thrones.http.gzip").indexOf("true") >= 0 || System.getProperty("thrones.http.gzip").indexOf("yes") >= 0;

    public static HttpCache initInstance() {
        HttpCache localInstance = instance;
        if (localInstance == null) {
            synchronized (HttpCache.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HttpCache();
                }
            }
        }
        return localInstance;
    }

    public static HttpCache getInstance() {
        HttpCache localInstance = instance;
        if (localInstance == null) {
            synchronized (HttpCache.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HttpCache();
                }
            }
        }
        return localInstance;
    }

    private HttpCache() {
        cacheMap = new ConcurrentHashMap();
    }

    public void initCache(Object caches, String webmethod) {
        if (caches instanceof List) {
            Webmethod wm = new Webmethod();
            for (Object s : (List) caches) {
                wm.caches.add(s.toString());
            }
            cacheMap.put(webmethod, wm);

        } else {
            Webmethod wm = new Webmethod();
            wm.caches.add(caches.toString());
            cacheMap.put(webmethod, wm);
        }
    }

    public boolean getCache(String webmethod, String params, HttpExchange ex) {

        OutputStreamWriter osw = null;

        try {

            Webmethod wm = cacheMap.get(webmethod);
            if (wm != null) {
                if (params != null) {
                    String result = wm.mappings.get(params);
                    if (result != null) {
                        if (gzip) {
                            ex.getResponseHeaders().set("Content-Encoding", "gzip");
                        }
                        ex.sendResponseHeaders(200, 0);

                        if (gzip) {
                            GZIPOutputStream gos = new GZIPOutputStream(ex.getResponseBody());
                            osw = new OutputStreamWriter(gos);
                        } else {
                            osw = new OutputStreamWriter(ex.getResponseBody());
                        }
                        osw.write(result);
                        osw.flush();
                        osw.close();
                        return true;
                    }
                } else {
                    if (wm.nullReference != null) {
                        if (gzip) {
                            ex.getResponseHeaders().set("Content-Encoding", "gzip");
                        }
                        ex.sendResponseHeaders(200, 0);
                        if (gzip) {
                            GZIPOutputStream gos = new GZIPOutputStream(ex.getResponseBody());
                            osw = new OutputStreamWriter(gos);
                        } else {
                            osw = new OutputStreamWriter(ex.getResponseBody());
                        }
                        osw.write(wm.nullReference);
                        osw.flush();
                        osw.close();
                        return true;
                    }
                }
            }
        } catch (IOException exc) {
            LOG.error("Error reading cache for " + webmethod + "... " + exc.getMessage());
        }
        return false;
    }

    public void putCache(String webmethod, String params, String value) {
        Webmethod wm = cacheMap.get(webmethod);
        if (wm != null) {
            if (params != null) {
                wm.mappings.put(params, value);
            } else {
                wm.nullReference = value;
            }
        }
    }

    public boolean existsCache(String webmethod) {
        return cacheMap.get(webmethod) != null;
    }

    public void clearCache(String cache_id) {
        for (Webmethod wm : cacheMap.values()) {
            for (String id : wm.caches) {
                if (cache_id.equalsIgnoreCase(id)) {
                    wm.nullReference = null;
                    wm.mappings.clear();
                    return;
                }
            }
        }
    }
}
