/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPOutputStream;
import ru.wnfx.scriptfighter.engine.core.Webmethod;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.SizedStack;

/**
 *
 * @author sisyandra
 */
final class MyEntry<K, V> implements Map.Entry<K, V> {
    private final K key;
    private V value;

    public MyEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        V old = this.value;
        this.value = value;
        return old;
    }
}

public class HttpSnapshot {

    
    private static volatile HttpSnapshot instance;
    private volatile ConcurrentHashMap<String, SizedStack<MyEntry<String, Map>>> cacheMap;
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(HttpSnapshot.class);
    boolean need_snaphot = System.getProperty("thrones.http.snapshots").indexOf("true") >= 0 || System.getProperty("thrones.http.snapshots").indexOf("yes") >= 0;
    private Gson gson;
    Map<Integer, String> codes;

    public static HttpSnapshot initInstance() {
        HttpSnapshot localInstance = instance;
        if (localInstance == null) {
            synchronized (HttpSnapshot.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HttpSnapshot();
                }
            }
        }
        return localInstance;
    }

    public static HttpSnapshot getInstance() {
        HttpSnapshot localInstance = instance;
        if (localInstance == null) {
            synchronized (HttpSnapshot.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HttpSnapshot();
                }
            }
        }
        return localInstance;
    }

    private HttpSnapshot() {
        cacheMap = new ConcurrentHashMap();
        gson = new Gson(); 
        codes = new HashMap();
        codes.put(400,"Bad Request");
        codes.put(401,"Unauthorized");
        codes.put(402,"Payment Required");
        codes.put(403,"Forbidden");
        codes.put(404,"Not Found");
        codes.put(405,"Method Not Allowed");
        codes.put(406,"Not Acceptable");
        codes.put(407,"Proxy Authentication Required");
        codes.put(408,"Request Timeout");
        codes.put(409,"Conflict");
        codes.put(410,"Gone");
        codes.put(411,"Length Required");
        codes.put(412,"Precondition Failed");
        codes.put(413,"Payload Too Large");
        codes.put(414,"URI Too Long");
        codes.put(415,"Unsupported Media Type");
        codes.put(416,"Range Not Satisfiable");
        codes.put(417,"Expectation Failed");
        codes.put(418,"I’m a teapot");
        codes.put(419,"Authentication Timeout");
        codes.put(421,"Misdirected Request");
        codes.put(422,"Unprocessable Entity");
        codes.put(423,"Locked");
        codes.put(424,"Failed Dependency");
        codes.put(425,"Too Early");
        codes.put(426,"Upgrade Required");
        codes.put(428,"Precondition Required");
        codes.put(429,"Too Many Requests");
        codes.put(431,"Request Header Fields Too Large");
        codes.put(449,"Retry With");
        codes.put(451,"Unavailable For Legal Reasons");
        codes.put(499,"Client Closed Request");
        codes.put(500,"Internal Server Error");
        codes.put(501,"Not Implemented");
        codes.put(502,"Bad Gateway");
        codes.put(503,"Service Unavailable");
        codes.put(504,"Gateway Timeout");
        codes.put(505,"HTTP Version Not Supported");
        codes.put(506,"Variant Also Negotiates");
        codes.put(507,"Insufficient Storage");
        codes.put(508,"Loop Detected");
        codes.put(509,"Bandwidth Limit Exceeded");
        codes.put(510,"Not Extended");
        codes.put(511,"Network Authentication Required");
        codes.put(520,"Unknown Error");
        codes.put(521,"Web Server Is Down");
        codes.put(522,"Connection Timed Out");
        codes.put(523,"Origin Is Unreachable");
        codes.put(524,"A Timeout Occurred");
        codes.put(525,"SSL Handshake Failed");
        codes.put(526,"Invalid SSL Certificate");
    }

    public void takeSnapshot(String method, Object request, Object response, int code, ArrayList<Map.Entry<String, List<String>>> headers, ArrayList<Map<String, String>> response_headers)
    {
        if(need_snaphot)
        {
            SizedStack<MyEntry<String, Map>> stackOfCalls = cacheMap.get(method);
            if(stackOfCalls==null)
            {
                stackOfCalls = new SizedStack(5);
                cacheMap.put(method, stackOfCalls);
            }
            Map<String, Object> data = new HashMap();
            data.put("code",code);
            data.put("request_headers",headers);
            data.put("response_headers",response_headers);
            if(response instanceof Map)
                data.put("response",gson.toJson(response, Map.class));
            if(response instanceof List)
                data.put("response",gson.toJson(response, List.class));
            
            if(request instanceof String)
                stackOfCalls.push(
                                new MyEntry(
                                                request, 
                                                data
                                            )
                            );
            else
                stackOfCalls.push(
                                new MyEntry(
                                                gson.toJson(ObjectUtils.convert(request), Map.class), 
                                                data
                                            )
                            );
            
            
            //stackOfCalls.add(e)
        }
    }
    
    public List<Map<String, Object>> getExamples(String method)
    {
        List<Map<String, Object>> examples = new ArrayList();

        SizedStack<MyEntry<String, Map>> stackOfCalls = cacheMap.get(method);
        if(stackOfCalls == null)
            return new ArrayList();
                    
        for(MyEntry<String, Map> en: stackOfCalls)
        {
            Map<String, Object> body = new HashMap();
            body.put("mode","raw");
            body.put("raw", en.getValue().get("response"));
            body.put("request", en.getKey());
            List<Map<String, String>> headers = new ArrayList();
            if((ArrayList<Map.Entry<String, List<String>>>)en.getValue().get("headers")!=null)
            {
                for(Map.Entry<String, List<String>> head: (ArrayList<Map.Entry<String, List<String>>>)en.getValue().get("headers"))
                {
                    Map<String, String> next_header = new HashMap();
                    next_header.put("key", head.getKey());
                    next_header.put("type", "text");
                    next_header.put("value", head.getValue().get(0));
                    headers.add(next_header);
                }
            }
            body.put("headers", headers);
            
            List<Map<String, String>> response_headers = new ArrayList();
            if((ArrayList<Map<String, String>>)en.getValue().get("response_headers")!=null)
            {
                for(Map<String, String> head: (ArrayList<Map<String, String>>)en.getValue().get("response_headers"))
                {
                    Map<String, String> next_header = new HashMap();
                    next_header.put("key", head.get("key"));
                    next_header.put("value", head.get("value"));
                    response_headers.add(next_header);
                }
            }
            body.put("response_headers", response_headers);
            body.put("code", en.getValue().get("code"));
            examples.add(body);
        }
        return examples;
    }
    
    public String getDesc(int code)
    {
        String desc = codes.get(code);
        return desc==null?"OK":desc;
    }
}
