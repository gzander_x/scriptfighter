/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import com.google.gson.Gson;
import thrones.*;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import ru.wnfx.scriptfighter.utils.StringUtils;
/**
 *
 * @author sergk
 */
public class RestfulFilter extends Filter {

    private ArrayBlockingQueue requests_queue;
    private String function_name;
    private String method;
    private Gson gson;
    private ArrayList<Map.Entry<String, List<String>>> headers;
    //private OutputStream outputStream;
    ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool;
    public static class MultiPart {
        public PartType type;
        public String contentType;
        public String name;
        public String filename;
        public String value;
        public byte[] bytes;
    }

    public enum PartType{
        TEXT,FILE
    }

    public static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(RestfulFilter.class);
    
    public RestfulFilter(ArrayBlockingQueue requests_queue, String function_name, String method, ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool)
    {
        this.requests_queue = requests_queue;
        this.function_name=function_name;
        this.method=method;
        this.responsePool = responsePool;
        this.headers = new ArrayList();
        this.gson = new Gson();
    }
    
    @Override
    public String description() {
        return "Parses the requested URI for parameters";
    }

    @Override
    public void doFilter(HttpExchange exchange, Chain chain)
        throws IOException 
    {
        //outputStream = exchange.getResponseBody();
        this.headers = new ArrayList();
        for (Map.Entry<String, List<String>> header : exchange.getRequestHeaders().entrySet()) {
            this.headers.add(header);
        }        
        parseGetParameters(exchange);
        parsePostParameters(exchange);
        parsePutParameters(exchange);
        parseOptionsParameters(exchange);
        parseDeleteParameters(exchange);
        
        
        chain.doFilter(exchange);
    }    
    
    private String getLocale(HttpExchange exchange)
    {
        String locale = null;
        for (Map.Entry<String, List<String>> header : exchange.getRequestHeaders().entrySet()) {
            if(header.getKey().equalsIgnoreCase("Language"))
            {
                if(header.getValue()!=null)
                {
                    if(header.getValue().size()>0)
                        locale = header.getValue().get(0).toUpperCase();
                }
            }
        }
        return locale;
    }

    private void parseGetParameters(HttpExchange exchange)
        throws UnsupportedEncodingException 
    {
        if ("get".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            Map<String, Object> parameters = new HashMap<String, Object>();
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            String context = exchange.getHttpContext().getPath();
            String url_over_context = requestedUri.getPath().replace(context, "");
            
            //need for check for valid JSON

            if(query!=null) 
            {
                if(URLDecoder.decode(query,"UTF-8").equalsIgnoreCase("help"))
                {
                    exchange.setAttribute("help", "true");
                    exchange.setAttribute("function", function_name);
                }
                else if(URLDecoder.decode(query,"UTF-8").equalsIgnoreCase("clear"))
                {
                    exchange.setAttribute("clear", "true");
                    exchange.setAttribute("function", function_name);
                }
                else
                {
                    //String path=exchange.getHttpContext().getPath();
                    String params = URLDecoder.decode(query,"UTF-8");
                    String locale = getLocale(exchange);
                    String paramsLocale = StringUtils.queryToJson(params)+(locale==null?"":("&locale="+locale));
                    
                    if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                    {
                        String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                        exchange.setAttribute("thread_no", thread_no); 
                        responsePool.put(thread_no, new ArrayBlockingQueue(10));
                        HTTPMethodTask task = new HTTPMethodTask(method, "GET", url_over_context, StringUtils.queryToJson(params), function_name, thread_no, headers);
                        task.outputStream = exchange;
                        requests_queue.offer(task);
                    }
                }
            }
            else
            {
                String locale = getLocale(exchange);
                String paramsLocale = ""+(locale==null?"":("&locale="+locale));
                //+(locale==null?"":("&locale="+locale))
                //    String paramsLocale = params+(locale==null?"":("&locale="+locale));
                if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                {
                    String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                    exchange.setAttribute("thread_no", thread_no); 
                    responsePool.put(thread_no, new ArrayBlockingQueue(10));
                    HTTPMethodTask task = new HTTPMethodTask(method, "GET", url_over_context,"",function_name, thread_no, headers);
                    task.outputStream = exchange;
                    requests_queue.offer(task);
                }
            }
        }
        //parseQuery(query, parameters);
        //exchange.setAttribute("parameters", parameters);
    }

    public static byte[] getInputAsBinary(InputStream requestStream) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            byte[] buf = new byte[100000];
            int bytesRead=0;
            while ((bytesRead = requestStream.read(buf)) != -1){
            //while (requestStream.available() > 0) {
            //    int i = requestStream.read(buf);
                bos.write(buf, 0, bytesRead);
            }
            requestStream.close();
            bos.close();
        } catch (IOException e) {
            LOG.error("error while decoding http input stream", e);
        }
        return bos.toByteArray();
    }

    /**
     * Search bytes in byte array returns indexes within this byte-array of all
     * occurrences of the specified(search bytes) byte array in the specified
     * range
     * borrowed from https://github.com/riversun/finbin/blob/master/src/main/java/org/riversun/finbin/BinarySearcher.java
     *
     * @param srcBytes
     * @param searchBytes
     * @param searchStartIndex
     * @param searchEndIndex
     * @return result index list
     */
    public List<Integer> searchBytes(byte[] srcBytes, byte[] searchBytes, int searchStartIndex, int searchEndIndex) throws UnsupportedEncodingException {
        int destSize = searchBytes.length;
        final List<Integer> positionIndexList = new ArrayList<Integer>();
        int cursor = searchStartIndex;
        for (int i=0; cursor < searchEndIndex + 1; i++) {
            int index = indexOf(srcBytes, searchBytes, cursor, searchEndIndex);
            if (index >= 0) {
                positionIndexList.add(index);
                cursor = index + destSize;
            } else {
                cursor++;
            }
            if(i==0)
            {
                destSize+=2;
                searchBytes = ("\r\n"+new String(searchBytes, "UTF-8")).getBytes("UTF-8");
            }  
        }
        return positionIndexList;
    }

    /**
     * Returns the index within this byte-array of the first occurrence of the
     * specified(search bytes) byte array.<br>
     * Starting the search at the specified index, and end at the specified
     * index.
     * borrowed from https://github.com/riversun/finbin/blob/master/src/main/java/org/riversun/finbin/BinarySearcher.java
     *
     * @param srcBytes
     * @param searchBytes
     * @param startIndex
     * @param endIndex
     * @return
     */
    public int indexOf(byte[] srcBytes, byte[] searchBytes, int startIndex, int endIndex) {
        if (searchBytes.length == 0 || (endIndex - startIndex + 1) < searchBytes.length) {
            return -1;
        }
        int maxScanStartPosIdx = srcBytes.length - searchBytes.length;
        final int loopEndIdx;
        if (endIndex < maxScanStartPosIdx) {
            loopEndIdx = endIndex;
        } else {
            loopEndIdx = maxScanStartPosIdx;
        }
        int lastScanIdx = -1;
        label: // goto label
        for (int i = startIndex; i <= loopEndIdx; i++) {
            for (int j = 0; j < searchBytes.length; j++) {
                if (srcBytes[i + j] != searchBytes[j]) {
                    continue label;
                }
                lastScanIdx = i + j;
            }
            if (endIndex < lastScanIdx || lastScanIdx - i + 1 < searchBytes.length) {
                // it becomes more than the last index
                // or less than the number of search bytes
                return -1;
            }
            return i;
        }
        return -1;
    }

    
    private void parsePostParameters(HttpExchange exchange)
        throws IOException 
    {
        
        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            String context = exchange.getHttpContext().getPath();
            String url_over_context = requestedUri.getPath().replace(context, "");
            
            Headers hed = exchange.getRequestHeaders();
            String contentType = ((hed.get("Content-type") != null) ? (hed.get("Content-type").toString()) : (null));
            ArrayList<MultiPart> list = new ArrayList();
            if(contentType==null)
                contentType = "application/json";
            if(contentType.contains("multipart/form-data"))
            {
                //found form data
                String boundary = contentType.substring(contentType.indexOf("boundary=")+9).replace("]", "");
                LOG.info("Boundary: "+boundary);
                // as of rfc7578 - prepend "\r\n--"
                LOG.info("Boundary search: "+("\r\n--" + boundary));
                byte[] boundaryBytes = (/*"\r\n--"*/"--" + boundary).getBytes(Charset.forName("UTF-8"));
                
                byte[] payload = getInputAsBinary(exchange.getRequestBody());
                String testbody = new String(payload, "UTF-8");
                LOG.info("Body: "+testbody);

                List<Integer> offsets = searchBytes(payload, boundaryBytes, 0, payload.length - 1);
                for(int idx=0;idx<offsets.size();idx++){
                    int startPart = offsets.get(idx);
                    int endPart = payload.length;
                    if(idx<offsets.size()-1){
                        endPart = offsets.get(idx+1);
                    }
                    byte[] part = Arrays.copyOfRange(payload,startPart,endPart);
                    //look for header
                    int headerEnd = indexOf(part,"\r\n\r\n".getBytes(Charset.forName("UTF-8")),0,part.length-1);
                    if(headerEnd>0) {
                        MultiPart p = new MultiPart();
                        byte[] head = Arrays.copyOfRange(part, 0, headerEnd);
                        String header = new String(head);
                        // extract name from header
                        int nameIndex = header.indexOf("\r\nContent-Disposition: form-data; name=");
                        if (nameIndex >= 0) {
                            int startMarker = nameIndex + 39;
                            //check for extra filename field
                            int fileNameStart = header.indexOf("; filename=");
                            if (fileNameStart >= 0) {
                                String filename = header.substring(fileNameStart + 11, header.indexOf("\r\n", fileNameStart));
                                p.filename = filename.replace('"', ' ').replace('\'', ' ').trim();
                                p.name = header.substring(startMarker, fileNameStart).replace('"', ' ').replace('\'', ' ').trim();
                                p.type = PartType.FILE;
                            } else {
                                int endMarker = header.indexOf("\r\n", startMarker);
                                if (endMarker == -1)
                                    endMarker = header.length();
                                p.name = header.substring(startMarker, endMarker).replace('"', ' ').replace('\'', ' ').trim();
                                p.type = PartType.TEXT;
                            }
                        } else {
                            // skip entry if no name is found
                            continue;
                        }
                        // extract content type from header
                        int typeIndex = header.indexOf("\r\nContent-Type:");
                        if (typeIndex >= 0) {
                            int startMarker = typeIndex + 15;
                            int endMarker = header.indexOf("\r\n", startMarker);
                            if (endMarker == -1)
                                endMarker = header.length();
                            p.contentType = header.substring(startMarker, endMarker).trim();
                        }

                        //handle content
                        if (p.type == PartType.TEXT) {
                            //extract text value
                            byte[] body = Arrays.copyOfRange(part, headerEnd + 4, part.length);
                            p.value = new String(body);
                        } else {
                            //must be a file upload
                            p.bytes = Arrays.copyOfRange(part, headerEnd + 4, part.length);
                        }
                        list.add(p);
                    }
                }
                Map<String, Object> result = new HashMap();
                for(int i=0; i<list.size(); i++)
                {
                    if(list.get(i).type == PartType.TEXT)
                    {
                        result.put(list.get(i).name, list.get(i).value);
                    }
                    else if(list.get(i).type == PartType.FILE)
                    {
                        result.put(list.get(i).name, list.get(i).bytes);
                    }
                }
                String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                exchange.setAttribute("thread_no", thread_no); 
                responsePool.put(thread_no, new ArrayBlockingQueue(10));
                HTTPMethodTask task = new HTTPMethodTask(method, "POST", url_over_context, result, function_name, thread_no, headers);
                task.outputStream = exchange;
                requests_queue.offer(task);       
            }
            else
            {
                @SuppressWarnings("unchecked")
                /*Map<String, Object> parameters =
                    (Map<String, Object>)exchange.getAttribute("parameters");*/
                InputStreamReader isr =
                    new InputStreamReader(exchange.getRequestBody(),"utf-8");
                BufferedReader br = new BufferedReader(isr);
                String result="";

                for(String tmp_result=br.readLine();tmp_result!=null;tmp_result=br.readLine())
                {
                    result+=tmp_result;
                }

                if(result.equalsIgnoreCase("help"))
                {
                    exchange.setAttribute("help", "true");
                    exchange.setAttribute("function", function_name);
                }
                else
                {
                    //need for check for valid JSON
                    String locale = getLocale(exchange);
                    if(query!=null)
                    {
                        String params = URLDecoder.decode(query,"UTF-8");
                        Object tmp_result = gson.fromJson(result, Object.class);
                        Map cumulative_result = gson.fromJson(StringUtils.queryToJson(params), Map.class);
                        cumulative_result.put("body", tmp_result);
                        result = gson.toJson(cumulative_result);
                    }
                    
                    String paramsLocale = result+(locale==null?"":("&locale="+locale));

                    if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                    {
                        String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                        exchange.setAttribute("thread_no", thread_no); 
                        responsePool.put(thread_no, new ArrayBlockingQueue(10));
                        HTTPMethodTask task = new HTTPMethodTask(method, "POST", url_over_context, result, function_name, thread_no, headers);
                        task.outputStream = exchange;
                        requests_queue.offer(task);
                    }
                }
            }
            
        }
    }
    
     private void parsePutParameters(HttpExchange exchange)
        throws IOException 
    {
        
        if ("put".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            String context = exchange.getHttpContext().getPath();
            String url_over_context = requestedUri.getPath().replace(context, "");
            
            Headers hed = exchange.getRequestHeaders();
            String contentType = ((hed.get("Content-type") != null) ? (hed.get("Content-type").toString()) : (null));
            ArrayList<MultiPart> list = new ArrayList();
            if(contentType==null)
                contentType = "application/json";
            if(contentType.contains("multipart/form-data"))
            {
                //found form data
                String boundary = contentType.substring(contentType.indexOf("boundary=")+9).replace("]", "");
                LOG.info("Boundary: "+boundary);
                // as of rfc7578 - prepend "\r\n--"
                LOG.info("Boundary search: "+("\r\n--" + boundary));
                byte[] boundaryBytes = (/*"\r\n--"*/"--" + boundary).getBytes(Charset.forName("UTF-8"));
                
                byte[] payload = getInputAsBinary(exchange.getRequestBody());
                String testbody = new String(payload, "UTF-8");
                LOG.info("Body: "+testbody);

                List<Integer> offsets = searchBytes(payload, boundaryBytes, 0, payload.length - 1);
                for(int idx=0;idx<offsets.size();idx++){
                    int startPart = offsets.get(idx);
                    int endPart = payload.length;
                    if(idx<offsets.size()-1){
                        endPart = offsets.get(idx+1);
                    }
                    byte[] part = Arrays.copyOfRange(payload,startPart,endPart);
                    //look for header
                    int headerEnd = indexOf(part,"\r\n\r\n".getBytes(Charset.forName("UTF-8")),0,part.length-1);
                    if(headerEnd>0) {
                        MultiPart p = new MultiPart();
                        byte[] head = Arrays.copyOfRange(part, 0, headerEnd);
                        String header = new String(head);
                        // extract name from header
                        int nameIndex = header.indexOf("\r\nContent-Disposition: form-data; name=");
                        if (nameIndex >= 0) {
                            int startMarker = nameIndex + 39;
                            //check for extra filename field
                            int fileNameStart = header.indexOf("; filename=");
                            if (fileNameStart >= 0) {
                                String filename = header.substring(fileNameStart + 11, header.indexOf("\r\n", fileNameStart));
                                p.filename = filename.replace('"', ' ').replace('\'', ' ').trim();
                                p.name = header.substring(startMarker, fileNameStart).replace('"', ' ').replace('\'', ' ').trim();
                                p.type = PartType.FILE;
                            } else {
                                int endMarker = header.indexOf("\r\n", startMarker);
                                if (endMarker == -1)
                                    endMarker = header.length();
                                p.name = header.substring(startMarker, endMarker).replace('"', ' ').replace('\'', ' ').trim();
                                p.type = PartType.TEXT;
                            }
                        } else {
                            // skip entry if no name is found
                            continue;
                        }
                        // extract content type from header
                        int typeIndex = header.indexOf("\r\nContent-Type:");
                        if (typeIndex >= 0) {
                            int startMarker = typeIndex + 15;
                            int endMarker = header.indexOf("\r\n", startMarker);
                            if (endMarker == -1)
                                endMarker = header.length();
                            p.contentType = header.substring(startMarker, endMarker).trim();
                        }

                        //handle content
                        if (p.type == PartType.TEXT) {
                            //extract text value
                            byte[] body = Arrays.copyOfRange(part, headerEnd + 4, part.length);
                            p.value = new String(body);
                        } else {
                            //must be a file upload
                            p.bytes = Arrays.copyOfRange(part, headerEnd + 4, part.length);
                        }
                        list.add(p);
                    }
                }
                Map<String, Object> result = new HashMap();
                for(int i=0; i<list.size(); i++)
                {
                    if(list.get(i).type == PartType.TEXT)
                    {
                        result.put(list.get(i).name, list.get(i).value);
                    }
                    else if(list.get(i).type == PartType.FILE)
                    {
                        result.put(list.get(i).name, list.get(i).bytes);
                    }
                }
                String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                exchange.setAttribute("thread_no", thread_no); 
                responsePool.put(thread_no, new ArrayBlockingQueue(10));
                HTTPMethodTask task = new HTTPMethodTask(method, "PUT", url_over_context, result, function_name, thread_no, headers);
                task.outputStream = exchange;
                requests_queue.offer(task);       
            }
            else
            {
                @SuppressWarnings("unchecked")
                /*Map<String, Object> parameters =
                    (Map<String, Object>)exchange.getAttribute("parameters");*/
                InputStreamReader isr =
                    new InputStreamReader(exchange.getRequestBody(),"utf-8");
                BufferedReader br = new BufferedReader(isr);
                String result="";

                for(String tmp_result=br.readLine();tmp_result!=null;tmp_result=br.readLine())
                {
                    result+=tmp_result;
                }

                if(result.equalsIgnoreCase("help"))
                {
                    exchange.setAttribute("help", "true");
                    exchange.setAttribute("function", function_name);
                }
                else
                {
                    //need for check for valid JSON
                    String locale = getLocale(exchange);
                    String paramsLocale = result+(locale==null?"":("&locale="+locale));

                    if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                    {
                        String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                        exchange.setAttribute("thread_no", thread_no); 
                        responsePool.put(thread_no, new ArrayBlockingQueue(10));
                        HTTPMethodTask task = new HTTPMethodTask(method, "PUT", url_over_context, result, function_name, thread_no, headers);
                        task.outputStream = exchange;
                        requests_queue.offer(task);
                    }
                }
            }
            
        }
    }
    
    private void parseDeleteParameters(HttpExchange exchange)
        throws IOException 
    {
        
        if ("delete".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
            @SuppressWarnings("unchecked")
            Map<String, Object> parameters =
                (Map<String, Object>)exchange.getAttribute("parameters");
            InputStreamReader isr =
                new InputStreamReader(exchange.getRequestBody(),"utf-8");
            BufferedReader br = new BufferedReader(isr);
            String result="";
            URI requestedUri = exchange.getRequestURI();
            String query = requestedUri.getRawQuery();
            String context = exchange.getHttpContext().getPath();
            String url_over_context = requestedUri.getPath().replace(context, "");

            for(String tmp_result=br.readLine();tmp_result!=null;tmp_result=br.readLine())
            {
                result+=tmp_result;
            }

            if(result.equalsIgnoreCase("help"))
            {
                exchange.setAttribute("help", "true");
                exchange.setAttribute("function", function_name);
            }
            else
            {
                //need for check for valid JSON
                String locale = getLocale(exchange);
                String paramsLocale = result+(locale==null?"":("&locale="+locale));
                    
                if(!HttpCache.getInstance().getCache(method, paramsLocale, exchange))
                {
                    String thread_no = java.util.UUID.randomUUID().toString().replace("-", "");
                    exchange.setAttribute("thread_no", thread_no); 
                    responsePool.put(thread_no, new ArrayBlockingQueue(10));
                    HTTPMethodTask task = new HTTPMethodTask(method, "DELETE", url_over_context, result, function_name, thread_no, headers);
                    task.outputStream = exchange;
                    requests_queue.offer(task);
                }
            }
        }
    }
    
    private void parseOptionsParameters(HttpExchange exchange)
        throws IOException 
    {
        
        if ("options".equalsIgnoreCase(exchange.getRequestMethod())) 
        {
           exchange.setAttribute("help", "true");
           exchange.setAttribute("function", function_name);
        }
            
    }
    

     @SuppressWarnings("unchecked")
     private void parseQuery(String query, Map<String, Object> parameters)
         throws UnsupportedEncodingException {

         if (query != null) {
             String pairs[] = query.split("[&]");

             for (String pair : pairs) {
                 String param[] = pair.split("[=]");

                 String key = null;
                 String value = null;
                 if (param.length > 0) {
                     key = URLDecoder.decode(param[0],
                         System.getProperty("file.encoding"));
                 }

                 if (param.length > 1) {
                     value = URLDecoder.decode(param[1],
                         System.getProperty("file.encoding"));
                 }

                 if (parameters.containsKey(key)) {
                     Object obj = parameters.get(key);
                     if(obj instanceof List<?>) {
                         List<String> values = (List<String>)obj;
                         values.add(value);
                     } else if(obj instanceof String) {
                         List<String> values = new ArrayList<String>();
                         values.add((String)obj);
                         values.add(value);
                         parameters.put(key, values);
                     }
                 } else {
                     parameters.put(key, value);
                 }
             }
         }
    }
}
