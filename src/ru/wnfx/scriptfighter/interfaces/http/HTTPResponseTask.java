/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import java.io.OutputStream;
import java.util.ArrayList;

/**
 *
 * @author sergk
 */
public class HTTPResponseTask {

    String function;
    String response_body;
    String method;
    int code;
    public OutputStream outputStream;
    ArrayList<HTTPHeader> headers;
    public volatile boolean headers_sent;

    public HTTPResponseTask() {
        headers = new ArrayList();
        this.code = 200;
        this.headers_sent = false;
    }

    public HTTPResponseTask(String method, String task, String function) {
        headers = new ArrayList();
        this.function = function;
        response_body = task;
        this.method = method;
        this.code = 200;
        this.headers_sent = false;
    }

    public HTTPResponseTask(String method, String task, String function, int code) {
        headers = new ArrayList();
        this.function = function;
        response_body = task;
        this.method = method;
        this.code = code;
        this.headers_sent = false;
    }

    public ArrayList<HTTPHeader> getHeaders() {
        return headers;
    }
    //public void addHeader(HTTPHeader h){headers.add(h);}
}
