/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.interfaces.http;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import ru.wnfx.scriptfighter.interfaces.Listener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadFactory;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.PoolExecutor;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;
import sun.org.mozilla.javascript.internal.Undefined;

/**
 *
 * @author sergk
 */
class WorkerThreadFactory implements ThreadFactory {
   private int counter = 0;
   private String prefix = "Rest thread ";

   public WorkerThreadFactory(String prefix) {
     this.prefix = prefix;
   }

   public Thread newThread(Runnable r) {
     return new Thread(r, prefix + "#" + counter++);
   }
}

public class HTTPListener extends Listener implements HttpHandler {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(HTTPListener.class);
    private int port;
    private String name;
    private ArrayList<HttpContext> contexts;
    private volatile ArrayBlockingQueue<HTTPMethodTask> requests_queue;
    private volatile ArrayBlockingQueue<HTTPResponseTask> response_queue;
    private int thread_count = ConfigUtils.getConfigInt("thrones.rest.threads", 3);

    private HttpContext main_context;
    private boolean isModern;
    HttpServer server;
    private PoolExecutor enginePool;
    private volatile ConcurrentHashMap<String, ArrayBlockingQueue<HTTPResponseTask>> responsePool;

    public HTTPListener(Map<String, Object> mappings, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, int port, Scriptable parent, boolean isModern) {
        this.isModern = isModern;
        requests_queue = new ArrayBlockingQueue(1000);
        response_queue = new ArrayBlockingQueue(1000);
        enginePool = new PoolExecutor(thread_count, this, message_queue, commands_queue, events_queue, incoming_events_queue);
        responsePool = new ConcurrentHashMap();
        contexts = new ArrayList();
        //this.event_name = event_name;
        //this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        this.port = port;

        server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(port), 10);
        } catch (IOException ex) {
            LOG.error("Error starting listener with name: " + name + "... " + ex.getMessage());
        }

        HttpHandler mainhandler = new HttpHandler() {
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                Gson gson = new Gson();
                StringWriter sw = new StringWriter();
                JsonWriter jw = new JsonWriter(sw);
                jw.beginObject();
                gson.toJson(Integer.valueOf(100/*get load factor*/), Integer.class, jw.name("load_factor"));
                jw.name("methods").beginArray();
                for (HttpContext c : contexts) {
                    jw.beginObject();
                    gson.toJson(c.getPath().replace("/", ""), String.class, jw.name("name"));
                    gson.toJson(c.getAttributes().get("cacheable"), Boolean.class, jw.name("cacheable"));
                    gson.toJson(c.getAttributes().get("clear_cache"), Boolean.class, jw.name("clear_cache"));
                    Object cache_id = c.getAttributes().get("cache_id");
                    if (cache_id instanceof String) {
                        gson.toJson(c.getAttributes().get("cache_id"), String.class, jw.name("cache_id"));
                    } else if (cache_id instanceof List) {
                        jw.name("cache_id").beginArray();
                        for (String s : (List<String>) cache_id) {
                            jw.value(s);
                        }
                        jw.endArray();
                    }
                    gson.toJson(c.getAttributes().get("impacts"), Boolean.class, jw.name("impacts"));
                    jw.endObject();
                }
                jw.endArray();
                jw.endObject();
                String response = sw.toString();
                jw.flush();
                sw.flush();
                jw.close();
                sw.close();
                exchange.sendResponseHeaders(200, response.length());
                exchange.getResponseBody().write(response.getBytes("UTF-8"));
                exchange.close();
            }
        };

        main_context = server.createContext("/", mainhandler);
        //main_context.getFilters().add(new RestfulFilter(requests_queue, ""));

        for (String webmethod : mappings.keySet()) {
            HttpContext context = server.createContext("/" + webmethod, this);
            Object mappin = ObjectUtils.convert(mappings.get(webmethod));
            if (mappin instanceof String) {
                context.getAttributes().put("cacheable", Boolean.FALSE);
                context.getAttributes().put("clear_cache", Boolean.FALSE);
                context.getFilters().add(new RestfulFilter(requests_queue, (String) mappin, webmethod, responsePool));
            } else if (mappin instanceof Map) {
                Object handler = ((Map) mappin).get("handler");
                if (handler instanceof Undefined) {
                    LOG.info("Adding listener for webmethod " + webmethod + "... No handler specified!");
                } else if (handler instanceof String) {
                    Object cach = ((Map) mappin).get("cacheable");
                    if (cach instanceof Undefined) {
                        LOG.info("Adding listener for webmethod " + webmethod + "... No caching policy specified!");
                    } else if (cach instanceof Boolean) {
                        boolean cache_politics = (Boolean) cach;
                        Object cach_id = ObjectUtils.convert(((Map) mappin).get("cache_id"));
                        if (cach_id instanceof Undefined) {
                            LOG.info("Adding listener for webmethod " + webmethod + "... No cache_id specified!");
                        } else if (cach_id instanceof String || cach_id instanceof List) {
                            Object cache_id = cach_id;//(cach_id instanceof String)?cach_id:ObjectUtils.getListFromNative((NativeArray)cach_id);
                            Object cach_impacts = ((Map) mappin).get("impacts");
                            HttpCache.getInstance().initCache(cache_id, webmethod);
                            if (cach_impacts instanceof Undefined) {
                                LOG.error("Adding listener for webmethod " + webmethod + "... No cache impact specified!");
                            } else if (cach_impacts instanceof Boolean) {
                                Boolean cache_impacts = (Boolean) cach_impacts;
                                context.getAttributes().put("clear_cache", Boolean.FALSE);
                                context.getAttributes().put("cacheable", cache_politics);
                                context.getAttributes().put("impacts", cache_impacts);
                                context.getAttributes().put("cache_id", cache_id);
                                context.getFilters().add(new RestfulFilter(requests_queue, (String) handler, webmethod, responsePool));
                            } else {
                                LOG.error("Adding listener for webmethod " + webmethod + "... Wrong impacts datatype");
                            }
                        } else {
                            LOG.error("Adding listener for webmethod " + webmethod + "... Wrong cache_id datatype");
                        }
                    } else {
                        LOG.error("Adding listener for webmethod " + webmethod + "... Wrong cacheable datatype");
                    }
                } else {
                    LOG.error("Adding listener for webmethod " + webmethod + "... Wrong handler datatype");
                }
            }

            contexts.add(context);
        }
        HttpContext context = server.createContext("/postman", this);
        WorkerThreadFactory wtf = new WorkerThreadFactory("Rest thread ");
        server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool(wtf));
        server.start();
    }

    public HTTPListener(String event_name, String function_name, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, int port, Scriptable parent, boolean isModern) {
        this.isModern = isModern;
        requests_queue = new ArrayBlockingQueue(1000);
        response_queue = new ArrayBlockingQueue(1000);
        enginePool = new PoolExecutor(thread_count, this, message_queue, commands_queue, events_queue, incoming_events_queue);
        responsePool = new ConcurrentHashMap();
        contexts = new ArrayList();
        this.event_name = event_name;
        this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        this.port = port;

        server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(port), 10);
        } catch (IOException ex) {
            LOG.error("Error starting listener with name: " + name + "... " + ex.getMessage());
        }

        HttpHandler mainhandler = new HttpHandler() {
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                Gson gson = new Gson();
                StringWriter sw = new StringWriter();
                JsonWriter jw = new JsonWriter(sw);
                jw.beginObject();
                gson.toJson(Integer.valueOf(100/*get load factor*/), Integer.class, jw.name("load_factor"));
                jw.name("methods").beginArray();
                for (HttpContext c : contexts) {
                    jw.value(c.getPath().replace("/", ""));
                }
                jw.endArray();
                jw.endObject();
                String response = sw.toString();
                jw.flush();
                sw.flush();
                jw.close();
                sw.close();
                exchange.sendResponseHeaders(200, response.length());
                exchange.getResponseBody().write(response.getBytes("UTF-8"));
                exchange.close();
            }
        };

        main_context = server.createContext("/", mainhandler);
        //main_context.getFilters().add(new RestfulFilter(requests_queue, ""));

        HttpContext context = server.createContext("/" + event_name, this);
        context.getFilters().add(new RestfulFilter(requests_queue, function_name, event_name, responsePool));
        contexts.add(context);
        WorkerThreadFactory wtf = new WorkerThreadFactory("Rest thread ");
        server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool(wtf));
        server.start();
    }

    @Override
    public void registerMethods(CommonEngine jsEngine) {

    }

    @Override
    public void registerEvents() {
        /*events.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers after user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"ProxyEventPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject passed through filter. Attached object is a HttpObject"));
        events.add(new Event(prefix+"ProxyEventDoesNotPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject is blocked by filter. Attached object is a blocked HttpObject"));*/
    }

    public String getPostman()
    {
        Map<String, Object> postmane = new HashMap();
        Map<String, Object> info = new HashMap();
        info.put("_postman_id", java.util.UUID.randomUUID());
        info.put("name", "Scriptfighter Autogenerated");
        info.put("schema", "https://schema.getpostman.com/json/collection/v2.1.0/collection.json");
        postmane.put("info", info);
        List<Map<String, Object>> item = new ArrayList();
        try
        {
            for(HttpContext context: contexts)
            {
                List<Map<String, Object>> snapshots = HttpSnapshot.getInstance().getExamples(context.getPath().replace("/", ""));

                //
                Map<String, Object> method = new HashMap();
                method.put("name", context.getPath().replace("/", ""));
                Map<String, Object> request = new HashMap();
                request.put("method", "POST");

                Map<String, Object> body = new HashMap();
                body.put("mode","raw");
                if(snapshots.size()>0)
                {
                    body.put("raw",snapshots.get(0).get("request"));
                    request.put("header", snapshots.get(0).get("headers"));
                }
                else
                {
                    body.put("raw","{}");
                    request.put("header", new ArrayList());
                }

                Map<String, Object> url = new HashMap();
                url.put("raw","{{url}}"+context.getPath());
                url.put("host","{{url}}");
                url.put("path",context.getPath().replace("/", ""));
                request.put("body", body);
                request.put("url", url);
                method.put("request", request);

                List<Map<String, Object>> examples = new ArrayList();

                int i=0; 
                for(Map<String, Object> snapshot: snapshots)
                {
                    Map<String, Object> example = new HashMap();
                    example.put("name", "Example #"+(i+1));
                    example.put("code", snapshot.get("code"));
                    example.put("status", HttpSnapshot.getInstance().getDesc((Integer)snapshot.get("code")));
                    example.put("_postman_previewlanguage", "json");
                    example.put("body", snapshot.get("raw"));

                    Map<String, Object> originalRequest = new HashMap();
                    originalRequest.put("method","POST");
                    originalRequest.put("header",snapshot.get("headers"));
                    Map<String, Object> original_body = new HashMap();
                    original_body.put("mode","raw");
                    original_body.put("raw", snapshot.get("request"));
                    originalRequest.put("body",original_body);
                    originalRequest.put("header",snapshot.get("headers"));
                    originalRequest.put("url", url);
                    example.put("originalRequest", originalRequest);
                    example.put("header", snapshot.get("response_headers"));
                    example.put("cookie", new ArrayList());
                    examples.add(example);
                    i++;
                }
                method.put("response", examples);
                item.add(method);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        postmane.put("item", item);
        List<Map<String, Object>> variables = new ArrayList();
        Map<String, Object> variable = new HashMap();
        variable.put("id", java.util.UUID.randomUUID());
        variable.put("key", "url");
        variable.put("value", "");
        variables.add(variable);
        postmane.put("variable", variables);
        postmane.put("protocolProfileBehavior", new HashMap());
        Gson gson = new Gson();
        return gson.toJson(postmane, Map.class);
    }
    
    @Override
    public void handle(HttpExchange exc) throws IOException {
        if(exc.getHttpContext().getPath().endsWith("postman"))
        {
            exc.getResponseHeaders().set("Content-Type", "application/octet-stream");
            exc.getResponseHeaders().set("Content-Disposition", "attachment;filename=\"ScriptFighter.postman.json\"");
            exc.sendResponseHeaders(200, 0);
            OutputStream os = exc.getResponseBody();
            os.write(getPostman().getBytes("UTF-8"));
            os.flush();
            os.close();
            exc.close();
        }
        else
        {
            boolean gzip = System.getProperty("thrones.http.gzip").indexOf("true") >= 0 || System.getProperty("thrones.http.gzip").indexOf("yes") >= 0;
            String thread_no = (String) exc.getAttribute("thread_no");
            String path = exc.getHttpContext().getPath();
                LOG.info("Context called: " + path);
                String is_help = (String) exc.getAttribute("help");
                String is_clear = (String) exc.getAttribute("clear");
                exc.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");

                LOG.info("Context processing. Thread#" + thread_no);
                PrintWriter out = new PrintWriter(exc.getResponseBody());
                if (is_help != null) {
                    exc.sendResponseHeaders(200, 0);
                    String func = (String) exc.getAttribute("function");
                    BasicScript bs = LibraryHolder.getInstance().getScript(func);
                    String script_body = bs == null ? "" : bs.getSource();
                    out.println(script_body);
                    exc.setAttribute("help", null);
                    out.close();
                    exc.close();
                } else if (is_clear != null) {
                    exc.sendResponseHeaders(200, 0);
                    if (applyCachePolitics(path.replace("/", ""), false)) {
                        out.println("ok");
                    } else {
                        out.println("no linked cache");
                    }
                    exc.setAttribute("clear", null);
                    out.close();
                    exc.close();
                }
                else if (gzip) {
                //exc.getResponseHeaders().set("Content-Encoding", "gzip");

                //exc.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
                //OutputStream os = exc.getResponseBody();

                LOG.info("Context processing. Thread#" + thread_no);
                /*HTTPResponseTask e=null;
            try 
            {
                e = (HTTPResponseTask)responsePool.get(thread_no).take();
                //e.
                for(Object o: exc.getRequestHeaders().keySet())
                {
                    LOG.info(o.toString()+":"+exc.getRequestHeaders().get(o).toString());
                }
                responsePool.remove(thread_no);
                LOG.info("Context processing for method: "+e.method+" and thread #"+thread_no);
                applyCachePolitics(e.method, true);
            } 
            catch (InterruptedException ex) 
            {
                LOG.error(ex.getMessage());
            }
            exc.sendResponseHeaders(e.code, 0);
            e.headers_sent = true;*/
                //os.write(StringUtils.compressGZIP(e.response_body));
                //os.flush();
                //os.close();
                //exc.close();
            } else {
                
                /*
            else
            {
              HTTPResponseTask e=null;
              try 
              {
                  for(Object o: exc.getRequestHeaders().keySet())
                  {
                     LOG.info(o.toString()+":"+exc.getRequestHeaders().get(o).toString());
                  }

                  e = (HTTPResponseTask)responsePool.get(thread_no).take();
                  exc.sendResponseHeaders(e.code, 0);
                  e.headers_sent = true;
                  responsePool.remove(thread_no);
                  applyCachePolitics(e.method, true);
              } 
              catch (InterruptedException ex) 
              {
                  LOG.error(ex.getMessage());
              }

              //out.println(e.response_body);
            }
                 */
                //out.close();
                //exc.close();
            }
        }

    }

    private boolean applyCachePolitics(String func, boolean take_impact_attention) {
        Object cache_id = null;//=(String)exc.getAttribute("cache_id");
        //List<String> cache_id_list=null;//=(String)exc.getAttribute("cache_id");
        Boolean impacts = false;
        Boolean clear_cache = false;
        for (HttpContext c : contexts) {
            if (c.getPath().replace("/", "").equalsIgnoreCase(func)) {
                if (take_impact_attention) {
                    impacts = (Boolean) c.getAttributes().get("impacts");
                    if (impacts != null) {
                        if (impacts) {
                            cache_id = c.getAttributes().get("cache_id");
                        }
                    }
                } else {
                    cache_id = c.getAttributes().get("cache_id");
                }

                Object o = c.getAttributes().get("clear_cache");

                if (o != null) {
                    if (o instanceof Boolean) {
                        if (((Boolean) o)) {
                            c.getAttributes().put("clear_cache", false);
                        }
                    }
                }
                break;
            }
        }
        if (cache_id != null) {
            for (HttpContext c : contexts) {
                Object c_cache_id = c.getAttributes().get("cache_id");
                if (c_cache_id != null) {
                    if (c_cache_id instanceof String) {
                        if (cache_id instanceof String) {
                            if (((String) c_cache_id).equalsIgnoreCase((String) cache_id)) {
                                c.getAttributes().put("clear_cache", true);
                            }
                        } else if (cache_id instanceof List) {
                            for (String c_c_cache_id : (List<String>) cache_id) {
                                if (c_c_cache_id.equalsIgnoreCase((String) c_cache_id)) {
                                    c.getAttributes().put("clear_cache", true);
                                }
                            }
                        }
                    } else if (c_cache_id instanceof List) {
                        if (cache_id instanceof String) {
                            for (String c_c_cache_id : (List<String>) c_cache_id) {
                                if (c_c_cache_id.equalsIgnoreCase((String) cache_id)) {
                                    c.getAttributes().put("clear_cache", true);
                                }
                            }
                        } else if (cache_id instanceof List) {
                            for (String c_c_cache_id : (List<String>) c_cache_id) {
                                for (String c_c_c_cache_id : (List<String>) cache_id) {
                                    if (c_c_cache_id.equalsIgnoreCase(c_c_c_cache_id)) {
                                        c.getAttributes().put("clear_cache", true);
                                    }
                                }
                                /*if(c_c_cache_id.equalsIgnoreCase((String)cache_id))
                                {
                                    c.getAttributes().put("clear_cache", true);
                                }*/
                            }
                        }
                    }

                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void listen() {
        Gson gson = new Gson();
        ThreadsHolder.getInstance().addThread(this);
        for (; !stop;) {
            try {
                LOG.info("wait for task... ");
                HTTPMethodTask e = (HTTPMethodTask) requests_queue.take();

                LOG.info("new task: " + e + " for thread: " + e.thread_no);
                if (e != null) {
                    LOG.info("new task with function: " + e.function);
                    if (e.function != null) {
                        this.status = "processing_event";
                        String script_body;
                        //if(!isModern)
                        //    script_body=/*Middleware.getScript(e.function)+*/";\n $("+e.function+"("+e.request_body+"));\n";
                        //else
                        //    script_body="$$("+e.function+"("+e.request_body+"));\n";
                        script_body = e.function + "(" + e.request_body + ")\n";

                        //Object o=enginePool.get(e.thread_no).runScript(script_body, 0, java.util.UUID.randomUUID().toString().replace("-", ""));

                        /*if(o instanceof String) 
                        {
                            responsePool.get(e.thread_no).add(new HTTPResponseTask(e.method,(String)o, e.function));
                        }
                        else 
                        {
                            responsePool.get(e.thread_no).add(new HTTPResponseTask(e.method,gson.toJson(o),e.function));
                        }*/
                        //enginePool.runScript(e.method, e.function, script_body, e.thread_no, responsePool, e.headers, e.outputStream);
                        enginePool.runScript(e, script_body, responsePool);

                        this.status = "listening";
                    }
                } else if (e.function == null && e.method == null && e.request_body == null) {
                    break;
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
                ex.printStackTrace();
                break;
            }
        }
        server.stop(0);
        //server.
        ThreadsHolder.getInstance().removeThread(this);
    }

    @Override
    public void stop() {
        LOG.info("Command STOP received for HTTP listener ID: " + id);
        stop = true;
        //server.stop(0);
        HTTPMethodTask e = new HTTPMethodTask(null, null, null, null, null, null, null);
        requests_queue.add(e);

    }

}
