/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.file.core;
import java.io.File;
import java.io.FilenameFilter;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.utils.FileReader;

/**
 *
 * @author sergk
 */
public class LimitedFilenameFilter implements FilenameFilter
{
    public static final Logger LOG = Logger.getLogger(LimitedFilenameFilter.class);
    
    public int comparison_counter;
    public int limit;
    public int offset;
    public String filter_content;
    public String filter_encoding;
    public String filter_text;
    public String[] extentions;
    
    public LimitedFilenameFilter(String filter_text, String extentions, String filter_content, String encoding, int limit, int offset)
    {
        this.limit=limit;
        this.offset=offset;
        this.filter_text=filter_text;
        if(extentions!=null)
        {
            this.extentions = extentions.split(",");
        }
        else
            this.extentions = null;
        
        this.filter_content = filter_content;
        this.filter_encoding = encoding;
    
        comparison_counter=0;
    }
    
    private boolean checkForExtention(String filename)
    {
        if(extentions!=null)
        {
            if(extentions.length>0)
            {
                for(String ext:extentions)
                {
                    if(filename.endsWith(ext)) {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
        
        return false;
    }
    
    private boolean checkForContent(File f)
    {
        if(filter_content!=null)
        {
            if(filter_content.length()>0)
            {
                return FileReader.matches(f, filter_encoding, filter_text);
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    
    @Override
    public boolean accept(File dir, String name) 
    {
        if(checkForExtention(name)&&checkForContent(dir))
        {
            if(offset>=0 && limit>0)
            {
                if(comparison_counter>=offset && comparison_counter<(offset+limit))
                {
                    comparison_counter++;
                    LOG.debug("true. check_file: "+name+", counter: "+comparison_counter+", limit: "+limit+", offset: "+offset);
                    return true;
                }
                else
                {
                    comparison_counter++;
                    LOG.debug("false. check_file: "+name+", counter: "+comparison_counter+", limit: "+limit+", offset: "+offset);
                    return false;
                }
            }
            else {
                return true;
            }
        }
        //comparison_counter++;
        return false;
    }
}
