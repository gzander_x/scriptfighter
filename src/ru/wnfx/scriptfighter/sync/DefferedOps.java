/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.wnfx.scriptfighter.sync;

import ru.wnfx.scriptfighter.interfaces.http.HTTPMethodTask;

/**
 *
 * @author sisyandra
*/
public interface DefferedOps {
    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @return 
     * @see     java.lang.Thread#run()
     */
    public abstract HTTPMethodTask getTask();
    public abstract void sendResult(Object result);
}
