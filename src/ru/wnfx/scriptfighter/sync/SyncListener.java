/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.sync;

import ru.wnfx.scriptfighter.interfaces.http.*;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import ru.wnfx.scriptfighter.interfaces.BasicInterface;
import ru.wnfx.scriptfighter.interfaces.Connectorparam;
import ru.wnfx.scriptfighter.interfaces.Listener;
import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import ru.wnfx.scriptfighter.engine.messaging.DebugCommand;
import ru.wnfx.scriptfighter.engine.messaging.EngineMessage;
import ru.wnfx.scriptfighter.engine.core.ScriptThread;
import ru.wnfx.scriptfighter.engine.messaging.Event;
import ru.wnfx.scriptfighter.engine.messaging.LimitedMessageQueue;
import ru.wnfx.scriptfighter.engine.messaging.RequestMessage;
import ru.wnfx.scriptfighter.utils.Base64;
import ru.wnfx.scriptfighter.utils.MyBase64;
import ru.wnfx.scriptfighter.engine.core.BasicFunctions;
import ru.wnfx.scriptfighter.engine.core.BasicScript;
import ru.wnfx.scriptfighter.engine.core.LibraryHolder;
import ru.wnfx.scriptfighter.engine.core.Middleware;
import ru.wnfx.scriptfighter.engine.core.ScriptCategory;
import ru.wnfx.scriptfighter.engine.core.ScriptStopException;
import ru.wnfx.scriptfighter.engine.core.ScriptTimestampTooOldException;
import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import ru.wnfx.scriptfighter.engine.core.Scriptable;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.engine.core.manager.EnginePool;
import ru.wnfx.scriptfighter.engine.core.manager.ScriptTask;
import ru.wnfx.scriptfighter.engine.core.manager.SunRhinoEngine;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import sun.org.mozilla.javascript.internal.Undefined;
import thrones.MasterPage;
import thrones.ParameterFilter;
import thrones.Thrones;

/**
 *
 * @author sergk
 */
public class SyncListener extends Listener
{
    private static final org.apache.log4j.Logger LOG=org.apache.log4j.Logger.getLogger(HTTPListener.class);
    private int port;
    private String name;
    private ArrayList<HttpContext> contexts;
    private volatile ArrayBlockingQueue<HTTPMethodTask> requests_queue;
    private volatile ArrayBlockingQueue<HTTPResponseTask> response_queue;
    
    HttpServer server;
    
    DefferedOps operationHandler;
    
    public SyncListener(DefferedOps ops, ArrayBlockingQueue events_queue, LimitedMessageQueue incoming_events_queue, LimitedMessageQueue message_queue, ArrayBlockingQueue commands_queue, Scriptable parent)
    {
        requests_queue=new ArrayBlockingQueue(1000);
        response_queue=new ArrayBlockingQueue(1000);
        contexts = new ArrayList();
        //this.event_name = event_name;
        //this.function_name = function_name;
        this.events_queue = events_queue;
        this.incoming_events_queue = incoming_events_queue;
        this.message_queue = message_queue;
        this.commands_queue = commands_queue;
        this.parent = parent;
        
        operationHandler = ops;
        
        jsEngine=EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
    }
    
    @Override
    public void registerMethods(CommonEngine jsEngine) 
    {

    }
    
    @Override
    public void registerEvents()
    {
        /*events.add(new Event(prefix+"BeforeWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers before user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"AfterWebConnectorUserParamsRequested","Event of "+prefix+" proxy interface. Event triggers after user params is requested. Attached object is full list of params"));
        events.add(new Event(prefix+"ProxyEventPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject passed through filter. Attached object is a HttpObject"));
        events.add(new Event(prefix+"ProxyEventDoesNotPassesFilterThrough","Event of "+prefix+" proxy interface. Event triggers after HttpObject is blocked by filter. Attached object is a blocked HttpObject"));*/
    }
    
          
    @Override
    public void listen()
    {
        Gson gson=new Gson();
        ThreadsHolder.getInstance().addThread(this);
        LOG.info("1");
        for(;!stop;)
        {
            LOG.info("2");
            try 
            {
                HTTPMethodTask e = operationHandler.getTask();//(HTTPMethodTask)requests_queue.take();
                LOG.info("3");
                if(e!=null)
                {
                    LOG.info("4");
                    if(e.function!=null)
                    {
                        LOG.info("5");
                        //jsEngine=EnginePool.getInstance().getEngine(message_queue, commands_queue, events_queue, incoming_events_queue, this);
                        LOG.info("6");
                        this.status="processing_event";
                        String script_body;

                        script_body="$$("+e.function+"("+e.request_body+"));\n";
                        
                        LOG.info("7");
                        Object o=jsEngine.runScript(script_body, 0, java.util.UUID.randomUUID().toString().replace("-", ""));
                        LOG.info("8");
                        /*if(o instanceof String) 
                        {
                            response_queue.add(new HTTPResponseTask(e.method,(String)o, e.function));
                        }
                        else 
                        {
                            response_queue.add(new HTTPResponseTask(e.method,gson.toJson(o),e.function));
                        }*/
                        operationHandler.sendResult(o);
                        LOG.info("9");
                        
                        this.status="listening";
                    }
                }
                else if(e.function==null&&e.method==null&&e.request_body==null)
                {
                    break;
                }
            } 
            catch (Exception ex) 
            {
                LOG.error(ex.getMessage());
                break;
            } 
            catch (ScriptStopException ex) 
            {
                Logger.getLogger(ScriptTask.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
            catch (ScriptTimestampTooOldException ex) {
                Logger.getLogger(SyncListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        server.stop(0);
        //server.
        ThreadsHolder.getInstance().removeThread(this);
    }
    
    @Override
    public void stop() 
    {
        LOG.info("Command STOP received for HTTP listener ID: "+id);
        stop=true;
        //server.stop(0);
        HTTPMethodTask e = new HTTPMethodTask(null, null, null, null, null, null, null);
        requests_queue.add(e);
        
    }
    
}
