/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.messaging;

/**
 *
 * @author sisyandra
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;

/**
 * Created by Toni on 30.09.2016.
 */
public class Messaging extends Library {


    /**
     * Google URL to use firebase cloud messenging
     */
    private final String URL_SEND = "https://fcm.googleapis.com/fcm/send";

    /**
     * STATIC TYPES
     */

    public final String TYPE_TO = "to";  // Use for single devices, device groups and topics
    public final String TYPE_CONDITION = "condition"; // Use for Conditions
    private Gson gson;
    /**
     * Your SECRET server key
     */
    private String FCM_SERVER_KEY = "<Enter your server key here>";


    public Messaging(CommonEngine g) 
    {
        prefix="Messaging";
        desc="Cloud messaging API";
        gson = new GsonBuilder().setPrettyPrinting().create();
        if(g!=null)
        {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }
    
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String serverKey", desc="sets server key")
    public Messaging setServerKey(String serverKey)
    {
        FCM_SERVER_KEY = serverKey;
        return this;
    }

    /**
     * Send notification
     * @param type
     * @param typeParameter
     * @param notificationObject
     * @return
     * @throws IOException
     */
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String type, String typeParameter, Map<String, Object> notificationObject", desc="sends notification")
    public String sendNotification(String type, String typeParameter, Map<String, Object> notificationObject) throws IOException {
        return sendNotifictaionAndData(type, typeParameter, notificationObject, null, null);
    }

    /**
     * Send data
     * @param type
     * @param typeParameter
     * @param dataObject
     * @return
     * @throws IOException
     */
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String type, String typeParameter, Map<String, Object> notificationObject", desc="sends only data")
    public String sendData(String type, String typeParameter, Map<String, Object> dataObject) throws IOException {
        return sendNotifictaionAndData(type, typeParameter, null, dataObject, null);
    }

    /**
     * Send notification and data
     * @param type
     * @param typeParameter
     * @param notificationObject
     * @param dataObject
     * @param collapseKey
     * @return
     * @throws IOException
     */
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String type, String typeParameter, Map<String, Object> notificationObject", desc="sends notification +  data")
    public String sendNotifictaionAndData(String type, String typeParameter, Map<String, Object> notificationObject, Map<String, Object> dataObject, String collapseKey) throws IOException {
        String result = null;
        if (type.equals(TYPE_TO) || type.equals(TYPE_CONDITION)) {
            Map<String, Object> sendObject = new HashMap();
            sendObject.put(type, typeParameter);
            result = sendFcmMessage(sendObject, notificationObject, dataObject, collapseKey);
        }
        return result;
    }

    /**
     * Send data to a topic
     * @param topic
     * @param dataObject
     * @return
     * @throws IOException
     */
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String type, String typeParameter, Map<String, Object> notificationObject", desc="sends data to topic")
    public String sendTopicData(String topic, Map<String, Object> dataObject) throws IOException{
        return sendData(TYPE_TO, "/topics/" + topic, dataObject);
    }

    /**
     * Send notification to a topic
     * @param topic
     * @param notificationObject
     * @return
     * @throws IOException
     */
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String type, String typeParameter, Map<String, Object> notificationObject", desc="sends notification to topic")
    public String sendTopicNotification(String topic, Map<String, Object> notificationObject) throws IOException{
        return sendNotification(TYPE_TO, "/topics/" + topic, notificationObject);
    }

    /**
     * Send notification and data to a topic
     * @param topic
     * @param notificationObject
     * @param dataObject
     * @return
     * @throws IOException
     */
    @JavaScriptMethod(type="interfaces", autocomplete=true, params="String type, String typeParameter, Map<String, Object> notificationObject", desc="sends data & notification to topic")
    public String sendTopicNotificationAndData(String topic, Map<String, Object> notificationObject, Map<String, Object> dataObject) throws IOException{
        return sendNotifictaionAndData(TYPE_TO, "/topics/" + topic, notificationObject, dataObject, null);
    }

    /**
     * Send a Firebase Cloud Message
     * @param sendObject - Contains to or condition
     * @param notificationObject - Notification Data
     * @param dataObject - Data
     * @return
     * @throws IOException
     */
    
    private String sendFcmMessage(Map<String, Object> sendObject, Map<String, Object> notificationObject, Map<String, Object> dataObject, String collapseKey) throws IOException {
        HttpPost httpPost = new HttpPost(URL_SEND);

        // Header setzen
        httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.setHeader("Authorization", "key=" + FCM_SERVER_KEY);

        if (notificationObject != null) sendObject.put("notification", notificationObject);
        if (dataObject != null) sendObject.put("data", dataObject);
        if (collapseKey != null) sendObject.put("collapse_key", collapseKey);

        String data = gson.toJson(sendObject, Map.class);//new String(gson.toJson(sendObject, Map.class).getBytes("UTF-8"), "ISO-8859-1");

        StringEntity entity = new StringEntity(data,"UTF-8");

        // JSON-Object übergeben
        httpPost.setEntity(entity);

        HttpClient httpClient = HttpClientBuilder.create().build();

        BasicResponseHandler responseHandler = new BasicResponseHandler();
        String response = (String) httpClient.execute(httpPost, responseHandler);

        return response;
    }

}
  