/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.html.core;

import ru.wnfx.scriptfighter.interfaces.WebConnector;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import org.jsoup.*;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
//import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import gui.ava.html.image.generator.HtmlImageGenerator;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import javax.net.ssl.HttpsURLConnection;
import ru.wnfx.scriptfighter.utils.StringUtils;

/**
 *
 * @author sergk
 */
public class Html extends Library {

    Document doc;
    Elements elements;
    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(Html.class);
    private String last_url;
    private String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36";
    private int delay;
    private String[] useragents = {
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.21pre) Gecko K-Meleon/1.7.0",
        "Mozilla/5.0 (X11; ; Linux i686; rv:1.9.2.20) Gecko/20110805",
        "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a3pre) Gecko/20070330",
        "Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16",
        "Opera/12.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.02",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36",
        "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 7.0; InfoPath.3; .NET CLR 3.1.40767; Trident/6.0; en-IN)",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A",
        "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25"
    };

    public Html(CommonEngine g) {
        doc = null;
        elements = null;
        prefix = "HTML";
        desc = "HTML Parse API";
        last_url = null;

        if (g != null) {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String url", desc = "save HTML as img")
    public void saveToJpeg(String URL) {
        //URL url = MyClass.class.getResource("myhtml.html");
        /*try
        {
            Dimension size = new Dimension(1024, 768);
            Image img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
            JFrame frame = new JFrame();
            frame.setUndecorated(true);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JEditorPane pane = new JEditorPane(URL) {
                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponent(g);
                }
            };
            frame.setSize(size);
            frame.add(pane);
            frame.setVisible(true);
            Graphics g = img.getGraphics();
            pane.paintAll(g);
            ImageIO.write((RenderedImage) img, "jpg", new FileOutputStream("myimg.jpg"));
            frame.setVisible(false);
            frame.dispose();
        }
        catch(IOException e)
        {
            LOG.error("Error converting html: "+e.getMessage());
        }*/
        HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
        String jopa = getString(URL);
        imageGenerator.loadHtml(jopa);
        imageGenerator.saveAsImage("hello-world.png");
        imageGenerator.saveAsHtmlWithMap("hello-world.html", "hello-world.png");
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String url", desc = "save HTML as PDF")
    public void saveToPdf(String URL) {
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        // step 2
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("pdf.pdf"));

            // step 3
            document.open();
            // step 4
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, Get(URL));
            //step 5
            document.close();
        } catch (IOException e) {
            LOG.error("Error converting html: " + e.getMessage());
        } catch (DocumentException e) {
            LOG.error("Error converting html: " + e.getMessage());
        }
        System.out.println("PDF Created!");
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String useragent", desc = "set different useragent")
    public Html pretendRandomUA() {
        userAgent = useragents[StringUtils.randomInt(1, 11)];
        return this;
    }

    public Html randomDelay() {
        try {
            Thread.sleep(StringUtils.randomInt(1, 15) * 1000);
        } catch (InterruptedException ex) {
            g.error("Can't delay: " + ex.getMessage());
        }
        return this;
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String url", desc = "open HTML from URL")
    public Html openUrl(String URL) {
        //check URL
        try {
            URL url = new URL(URL);
        } catch (MalformedURLException ex) {
            g.error("Malformed URL: " + URL + ", exception: " + ex.getMessage());
        }

        try {
            doc = Jsoup.connect(URL).referrer("http://www.google.com").userAgent(userAgent).timeout(15000).get();
        } catch (IOException ex) {
            g.error("Can't open URL: " + URL + ", exception: " + ex.getMessage());
            try {
                Thread.sleep(1000);
                Connection con = Jsoup.connect(URL).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(10000);
                Connection.Response resp = con.execute();
                if (resp.statusCode() == 200) {
                    doc = con.get();
                }
            } catch (Exception ex1) {

                g.error("Can't open URL again: " + URL + ", exception: " + ex.getMessage());
            }

        }

        return this;
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String page", desc = "open HTML from parameter")
    public Html openHTML(String page) {

        doc = Jsoup.parse(page);
        return this;
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "String selector", desc = "applies selector to")
    public Html useSelector(String selector) {
        if (elements == null) {
            elements = doc.select(selector);
        } else {
            elements = elements.select(selector);
        }
        return this;
    }

    @JavaScriptMethod(type = "common", autocomplete = true, params = "", desc = "returns all of nodes")
    public Object getResult() {
        if (elements != null) {
            ArrayList<Object> tags_list = new ArrayList();
            for (Element e : elements) {

                HashMap<String, Object> element_obj = new HashMap();
                element_obj.put("class", e.className());
                element_obj.put("id", e.id());
                element_obj.put("name", e.nodeName());
                element_obj.put("tag", e.tagName());
                element_obj.put("val", e.val());
                element_obj.put("html", e.html());
                element_obj.put("text", e.text());
                Map<String, String> attrs = new HashMap();
                Iterator<Attribute> itr = e.attributes().iterator();

                while (itr.hasNext()) {
                    Attribute elt = itr.next();
                    attrs.put(elt.getKey(), elt.getValue());
                }
                element_obj.put("attrs", attrs);

                tags_list.add(ObjectUtils.convertNative(element_obj));
                //nobj.
            }
            elements = null;
            return ObjectUtils.convertNative(tags_list);
            /*NativeArray nobj;// = new NativeArray(elements.size());
            Object[] objs=new Object[elements.size()];
            int i=0;
            for (Element e: elements) 
            {
                NativeObject element_nobj = new NativeObject();
                element_nobj.defineProperty("class", e.className(), NativeObject.READONLY);
                element_nobj.defineProperty("id", e.id(), NativeObject.READONLY);
                element_nobj.defineProperty("name", e.nodeName(), NativeObject.READONLY);
                element_nobj.defineProperty("tag", e.tagName(), NativeObject.READONLY);
                element_nobj.defineProperty("val", e.val(), NativeObject.READONLY);
                element_nobj.defineProperty("html", e.html(), NativeObject.READONLY);
                element_nobj.defineProperty("text", e.text(), NativeObject.READONLY);

                //element_nobj.defineProperty("class", e.className(), NativeObject.READONLY);
                
                
                NativeObject attributes_nobj = new NativeObject();
                for(Attribute a: e.attributes())
                {
                    attributes_nobj.defineProperty(a.getKey(), a.getValue(), NativeObject.READONLY);
                }
                element_nobj.defineProperty("attributes", attributes_nobj, NativeObject.READONLY);
                
                //nobj.put(i, nobj, element_nobj);
                objs[i]=element_nobj;
                i++;
                //nobj.
            }
            nobj = new NativeArray(objs);
            return nobj;*/
        }
        return null;
    }

    public InputStream Get(String Url) {

        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL url;
        try {
            url = new URL(Url);
            HashMap<String, String> headers = new HashMap();
            int status = 0;
            URLConnection conn = Url.startsWith("https:") ? (HttpsURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection();

            for (int i = 1; conn.getHeaderFieldKey(i) != null; i++) {
                headers.put(conn.getHeaderFieldKey(i), conn.getHeaderField(i));
            }

            InputStream is = null;
            try {
                is = conn.getInputStream();
            } catch (IOException ex) {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            Map<String, List<String>> gogno = conn.getHeaderFields();

            if ("gzip".equals(conn.getContentEncoding())) {
                try {
                    is = new GZIPInputStream(is);
                } catch (IOException ex) {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String contentType = conn.getHeaderField("Content-Type");
            String charset = null;

            if (contentType != null) {
                for (String param : contentType.replace(" ", "").split(";")) {
                    if (param.startsWith("charset=")) {
                        charset = param.split("=", 2)[1];
                        break;
                    }
                }
            }

            BufferedReader rd;

            if (charset != null) {
                try {
                    rd = new BufferedReader(new InputStreamReader(is, charset));
                } catch (UnsupportedEncodingException ex) {
                    LOG.error("Wrong encoding: " + ex.getMessage());
                    rd = new BufferedReader(new InputStreamReader(is));
                }
            } else {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));

            String line;
            StringBuffer response = new StringBuffer();
            try {
                //java.io.By
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    //response.append('\r');
                }
            } catch (IOException ex) {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                rd.close();
            } catch (IOException ex) {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            final Document document = Jsoup.parse(response.toString());
            document.select("script").remove();
            document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);

            return new ByteArrayInputStream(document.html().getBytes(StandardCharsets.UTF_8));
            //return response.toString();
        } catch (MalformedURLException ex) {
            LOG.error("Wrong URL: " + ex.getMessage());
        } catch (IOException ex) {
            LOG.error("Some IO trouble: " + ex.getMessage());
        }

        return null;
    }

    public String getString(String Url) {

        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URL url;
        try {
            url = new URL(Url);
            HashMap<String, String> headers = new HashMap();
            int status = 0;
            URLConnection conn = Url.startsWith("https:") ? (HttpsURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection();

            for (int i = 1; conn.getHeaderFieldKey(i) != null; i++) {
                headers.put(conn.getHeaderFieldKey(i), conn.getHeaderField(i));
            }

            InputStream is = null;
            try {
                is = conn.getInputStream();
            } catch (IOException ex) {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            Map<String, List<String>> gogno = conn.getHeaderFields();

            if ("gzip".equals(conn.getContentEncoding())) {
                try {
                    is = new GZIPInputStream(is);
                } catch (IOException ex) {
                    Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String contentType = conn.getHeaderField("Content-Type");
            String charset = null;

            if (contentType != null) {
                for (String param : contentType.replace(" ", "").split(";")) {
                    if (param.startsWith("charset=")) {
                        charset = param.split("=", 2)[1];
                        break;
                    }
                }
            }

            BufferedReader rd;

            if (charset != null) {
                try {
                    rd = new BufferedReader(new InputStreamReader(is, charset));
                } catch (UnsupportedEncodingException ex) {
                    LOG.error("Wrong encoding: " + ex.getMessage());
                    rd = new BufferedReader(new InputStreamReader(is));
                }
            } else {
                rd = new BufferedReader(new InputStreamReader(is));
            }
            //GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (tBytes));

            String line;
            StringBuffer response = new StringBuffer();
            try {
                //java.io.By
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    //response.append('\r');
                }
            } catch (IOException ex) {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                rd.close();
            } catch (IOException ex) {
                Logger.getLogger(WebConnector.class.getName()).log(Level.SEVERE, null, ex);
            }

            return response.toString();
            //return response.toString();
        } catch (MalformedURLException ex) {
            LOG.error("Wrong URL: " + ex.getMessage());
        } catch (IOException ex) {
            LOG.error("Some IO trouble: " + ex.getMessage());
        }

        return "";
    }

}
