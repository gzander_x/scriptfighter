/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;

import ru.wnfx.scriptfighter.db.nosql.NOSQL;

/**
 *
 * @author sergk
 */
public class OuterJoin extends Join {

    public OuterJoin(NOSQL view) {
        this.view = view;
        //this.conditions=conditions;
    }

    public OuterJoin(String cortej) {
        this.relation = cortej;
        //this.conditions=conditions;
    }

    @Override
    public String toSQL(String rel) {
        String conditions_text;
        return " left outer join " + ((this.relation == null) ? ("(" + view.formatQuery() + ") " + view.relation) : relation) + " on (" + formatConditions(rel) + ")";
    }
}
