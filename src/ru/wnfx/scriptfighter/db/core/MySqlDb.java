/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import thrones.*;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.db.persistent.queue.*;
import ru.wnfx.scriptfighter.utils.db_helpers.ConnectionFactory;
import ru.wnfx.scriptfighter.utils.db_helpers.DB_Exception;

/**
 *
 * @author sergk
 */
public class MySqlDb extends InnerDatabase {

    public static final Logger LOG = Logger.getLogger(MySqlDb.class);
    private static volatile PersistentQueue<Transaction> statementsQueue;
    private static TransactThread thread;
    public volatile boolean blocker;

    private static MySqlDb instance;

    private String driver = System.getProperty("thrones.db.external.driver");
    private String cnString = System.getProperty("thrones.db.external.cnstring");
    private String user = System.getProperty("thrones.db.external.user");
    private String password = System.getProperty("thrones.db.external.password");
    public ConnectionFactory db_connect = null;

    public static MySqlDb initInstance(String s) {
        MySqlDb localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MySqlDb();
                }
            }
        }
        return localInstance;
    }

    public static MySqlDb getInstance() {
        MySqlDb localInstance = instance;
        if (localInstance == null) {
            synchronized (PathHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MySqlDb();
                }
            }
        }
        return localInstance;
    }

    private MySqlDb() {

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException Ex) {

        }
        connectionString = cnString;

        LOG.debug("DBHelper created: " + connectionString);
        try {
            //connection = DriverManager.getConnection(connectionString); 
            db_connect = new ConnectionFactory(driver, cnString, user, password, "");

            // для PostgreSQL сначала нужно создать транзакцию (AutoCommit == false)...
            try {
                db_connect.connect();
                //db_connect.connection.setAutoCommit(false);
            } catch (DB_Exception dbe) {
                LOG.error("Connect failed: " + dbe.getMessage());
            }
            connection = db_connect.connection;

        } catch (Exception e) {
            // if the error message is "out of memory", 
            // it probably means no database file is found
            LOG.error("Error running select:" + e.getMessage());
        }
    }

    @Override
    public ResultSet runSelect(String select_text) {
        LOG.debug("try to query: " + select_text);

        ResultSet result = null;
        try {
            LOG.debug("1");
            Statement statement = connection.createStatement();
            LOG.debug("2");
            result = statement.executeQuery(select_text);
            LOG.debug("3");
        } catch (SQLException e) {
            // if the error message is "out of memory", 
            // it probably means no database file is found
            LOG.error("Error running select:" + e.getMessage());
            try {
                    if(connection.isClosed())
                    {
                        LOG.error("Try to reconnect");
                        try {
                            db_connect.connect();
                            //db_connect.connection.setAutoCommit(false);
                        } catch (DB_Exception dbe) {
                            LOG.error("Reconnect failed: " + dbe.getMessage());
                        }
                        connection = db_connect.connection;
                    }
            } catch (SQLException ee) {
                LOG.error("Error re:" + ee.getMessage());
            }
        } catch (Exception e) {
            // if the error message is "out of memory", 
            // it probably means no database file is found
            LOG.error("Error running select EEE:" + e.getMessage());
        } finally {

        }
        return result;
    }

    @Override
    public void closeDatabase() {
        /*try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/

    }

    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int runInsertUpdate(String insert_text) {
        int result = 0;
        LOG.debug("try to run: " + insert_text);

        try {
            try {
                // create a database connection
                Statement statement = connection.createStatement();
                statement.executeUpdate(insert_text);
                result = statement.getUpdateCount();
                statement.close();
            } catch (SQLException e) {
                // if the error message is "out of memory", 
                // it probably means no database file is found
                if(connection.isClosed())
                {
                    LOG.error("Try to reconnect");
                    try {
                        db_connect.connect();
                        //db_connect.connection.setAutoCommit(false);
                    } catch (DB_Exception dbe) {
                        LOG.error("Reconnect failed: " + dbe.getMessage());
                    }
                    connection = db_connect.connection;
                }
                LOG.error("Statement:" + insert_text + "Error running statement:" + e.getMessage());
            } finally {
            }
        } catch (Exception ex) {
            LOG.error("Error putting transaction into queue" + ex.getMessage());
        }

        return result;
    }

    @Override
    public boolean isTableExists(String tableName) {
        LOG.debug("try to check_table: " + tableName);

        boolean res = false;
        ResultSet result = null;
        try {
            // create a database connection
            Statement statement = connection.createStatement();

            result = statement.executeQuery("SELECT 'table' as type, table_name as name, table_name as tbl_name FROM information_schema.tables where table_name = '" + tableName + "'");
            while (result.next()) {
                // read the result set
                res = true;
            }

            result.close();
        } catch (/*SQL*/Exception e) {
            // if the error message is "out of memory", 
            // it probably means no database file is found
            LOG.error("Error running select:" + e.getMessage());
        } finally {
            /*try
            {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e)
            {
              // connection close failed.
              LOG.error("Error closing: "+e);
            }*/
        }
        return res;
    }
}
