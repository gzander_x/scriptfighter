/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.nosql;

import ru.wnfx.scriptfighter.db.sorts.SortDesc;
import ru.wnfx.scriptfighter.db.sorts.Sort;
import ru.wnfx.scriptfighter.db.sorts.SortAsc;
import ru.wnfx.scriptfighter.db.aggregates.SumAggregate;
import ru.wnfx.scriptfighter.db.aggregates.MaxAggregate;
import ru.wnfx.scriptfighter.db.aggregates.MinAggregate;
import ru.wnfx.scriptfighter.db.aggregates.Aggregate;
import ru.wnfx.scriptfighter.db.aggregates.AvgAggregate;
import ru.wnfx.scriptfighter.db.conditions.MoreCondition;
import ru.wnfx.scriptfighter.db.conditions.EqualsCondition;
import ru.wnfx.scriptfighter.db.conditions.LessEqualsCondition;
import ru.wnfx.scriptfighter.db.conditions.LikeCondition;
import ru.wnfx.scriptfighter.db.conditions.Condition;
import ru.wnfx.scriptfighter.db.conditions.LessCondition;
import ru.wnfx.scriptfighter.db.conditions.MoreEqualsCondition;
import ru.wnfx.scriptfighter.db.core.DBHelper;
import ru.wnfx.scriptfighter.db.core.Param;
import ru.wnfx.scriptfighter.db.core.Serializer;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import ru.wnfx.scriptfighter.db.conditions.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import ru.wnfx.scriptfighter.db.aggregates.*;
import ru.wnfx.scriptfighter.db.sorts.*;
import ru.wnfx.scriptfighter.db.core.*;
import ru.wnfx.scriptfighter.engine.core.AnnotParser;
import ru.wnfx.scriptfighter.engine.core.JavaScriptMethod;
import ru.wnfx.scriptfighter.engine.core.Library;
import ru.wnfx.scriptfighter.engine.core.manager.CommonEngine;
import ru.wnfx.scriptfighter.utils.ConfigUtils;
import ru.wnfx.scriptfighter.utils.ObjectUtils;
import ru.wnfx.scriptfighter.utils.StringUtils;

/**
 *
 * @author sergk
 */
public class NOSQL extends Library {

    private static String query_body;
    private ArrayList<Condition> conditions;
    private ArrayList<Param> pars;
    private ArrayList<Aggregate> aggs;
    private ArrayList<Sort> sorts;
    private ArrayList<String> fields;
    private ArrayList<Over> overs;
    private ArrayList<String> excluded_columns;
    public String relation;
    private boolean next_not;
    private boolean next_or;
    private boolean join;
    private int pagesize;
    private int pageno;
    private NOSQL subquery;
    private ArrayList<Join> joins;
    private List<Map<String, Object>> dataSet;
    private String locale;

    //private static volatile NOSQL instance;
    public static final Logger LOG = Logger.getLogger(NOSQL.class);

    private NOSQL(String cortej) {
        conditions = new ArrayList();
        pars = new ArrayList();
        aggs = new ArrayList();
        sorts = new ArrayList();
        fields = new ArrayList();
        joins = new ArrayList();
        locale = null;
        LOG.info("First init menu");
        relation = cortej;

        InnerDatabase h = null;

        if (ConfigUtils.getConfigBoolean("thrones.db.use_trans", false)) {
            h = MemoryDb.initInstance("");
        } else {
            if (ConfigUtils.getConfigString("thrones.db.database", "postgres").equalsIgnoreCase("postgres")) {
                h = PostgresDb.initInstance("");
            } 
            else if (ConfigUtils.getConfigString("thrones.db.database", "postgres").equalsIgnoreCase("mysql")) {
                h = MySqlDb.initInstance("");
            }
            else {
                h = new DBHelper();
            }
        }
        ResultSet rs = h.runSelect("select column_name,data_type from metadata where table_name='" + relation + "'");

        try {
            for (; rs.next();) {
                pars.add(new Param(rs.getString(1), "", rs.getString(2)));
            }
            rs.close();
            h.closeDatabase();
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }

        next_not = false;
        next_or = false;
        join = false;
        pagesize = 0;
        pageno = 0;
    }

    private String protectNull(Object t) {
        if (t == null) {
            return null;
        } else {
            return t.toString();
        }
    }

    public NOSQL(CommonEngine g) {
        prefix = "DB";
        desc = "inline database API";
        if (g != null) {
            //this.message_queue = g.message_queue;
            this.g = g;
            g.put(prefix, this);
        }
        methods = AnnotParser.dumpMethods(this);
        conditions = new ArrayList();
        pars = new ArrayList();
        aggs = new ArrayList();
        sorts = new ArrayList();
        fields = new ArrayList();
        joins = new ArrayList();
        locale = null;
        LOG.info("First init NOSQL");
    }

    public static String identifyType(String param_type) {
        if (param_type.equalsIgnoreCase("integer") || param_type.equalsIgnoreCase("numeric") || param_type.equalsIgnoreCase("float")) {
            return "DOUBLE";
        } else {
            return "UNKNOWN";
        }
    }

    public void refineParams() {
        for (int i = 0; i < pars.size(); i++) {
            Param p = pars.get(i);

            if (p.type.equalsIgnoreCase("UNKNOWN")) {
                DBHelper h = new DBHelper();
                ResultSet rs = h.runSelect("select " + p.name + " from " + relation);

                try {
                    for (; rs.next();) {
                        if (rs.getString(1) != null) {
                            if (rs.getString(1).length() > 0) {
                                if (!StringUtils.isJSONValid(rs.getString(1))) {
                                    p.type = "TEXT";
                                    rs.close();
                                    h.closeDatabase();
                                    break;
                                }
                            }
                        }
                    }
                    rs.close();
                    h.closeDatabase();
                } catch (SQLException ex) {
                    LOG.info("Error checking column: " + ex.getMessage());
                }
            }

            if (p.type.equalsIgnoreCase("UNKNOWN")) {
                p.type = "COMPLEX";
            }

            pars.set(i, p);
        }

        DBHelper h = new DBHelper();
        h.runInsertUpdate("delete from metadata where table_name='" + relation + "'");
        h.closeDatabase();

        for (Param p : pars) {
            h.runInsertUpdate("insert into metadata(table_name,column_name,data_type) values('" + relation + "','" + p.name + "','" + p.type + "')");
            h.closeDatabase();
        }
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String relation", desc = "inits interaction with table")
    public NOSQL get(String cortej) {
        conditions = new ArrayList();
        locale = null;
        pars = new ArrayList();
        aggs = new ArrayList();
        sorts = new ArrayList();
        fields = new ArrayList();
        joins = new ArrayList();
        LOG.info("start get from " + cortej + " for instance " + this.g.id);
        relation = cortej;
        subquery = null;

        InnerDatabase h = null;

        if (ConfigUtils.getConfigBoolean("thrones.db.use_trans", false)) {
            h = MemoryDb.initInstance("");
        } else {
            if (ConfigUtils.getConfigString("thrones.db.database", "postgres").equalsIgnoreCase("postgres")) {
                h = PostgresDb.initInstance("");
            } 
            else if (ConfigUtils.getConfigString("thrones.db.database", "postgres").equalsIgnoreCase("mysql")) {
                h = MySqlDb.initInstance("");
            }
            else {
                h = new DBHelper();
            }
        }
        //MemoryDb h=MemoryDb.initInstance("");
        ResultSet rs = h.runSelect("select column_name,data_type from metadata where table_name='" + relation + "'");
        boolean has_metadata = false;
        try {
            for (; rs.next();) {
                Param p = new Param(rs.getString(1), "", rs.getString(2));
                p.relation = relation;
                pars.add(p);
                has_metadata = true;
            }
            rs.close();
            h.closeDatabase();
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!has_metadata) {
            rs = h.runSelect("PRAGMA table_info('" + relation + "');");
            try {
                for (; rs.next();) {
                    Param p = new Param(rs.getString(2), "", identifyType(rs.getString(3)));
                    p.relation = relation;
                    pars.add(p);
                }
                rs.close();
                h.closeDatabase();
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }

            refineParams();
        }

        next_not = false;
        join = false;
        next_or = false;
        pagesize = 0;
        pageno = 0;

        LOG.info("finish get from " + cortej + " for instance " + this.g.id);

        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "", desc = "pushes result as subquery")
    public NOSQL push() {
        NOSQL tmp = new NOSQL(this.g).get(this.relation);
        tmp.subquery = this;
        return tmp;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "DB relation", desc = "joins with result")
    public NOSQL join(NOSQL rel) {
        join = true;
        joins.add(new InnerJoin(rel));
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String relation", desc = "joins with relation")
    public NOSQL join(String rel) {
        join = true;
        joins.add(new InnerJoin(rel));
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "DB relation", desc = "makes outer join with result")
    public NOSQL outerjoin(NOSQL rel) {
        join = true;
        joins.add(new OuterJoin(rel));
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String relation", desc = "makes outer join with relation")
    public NOSQL outerjoin(String rel) {
        join = true;
        joins.add(new OuterJoin(rel));
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "query 'equals' condition")
    public NOSQL equals(String key, Object value) {
        Param par = new Param(key, protectNull(value), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        Condition c = (new EqualsCondition(par, protectNull(value))).setNot(next_not).setOr(next_or);
        if (!join) {
            conditions.add(c);
        } else {
            if (joins.size() > 0) {
                joins.get(joins.size() - 1).addCondition(c);
            }
        }
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "query 'equals' condition")
    public NOSQL equals(String relation, String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        Condition c = (new EqualsCondition(relation, par, value.toString())).setNot(next_not).setOr(next_or);
        if (!join) {
            conditions.add(c);
        } else {
            if (joins.size() > 0) {
                joins.get(joins.size() - 1).addCondition(c);
            }
        }
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "query 'notequals' condition")
    public NOSQL notequals(String relation, String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        Condition c = (new NotEqualsCondition(relation, par, value.toString())).setNot(next_not).setOr(next_or);
        if (!join) {
            conditions.add(c);
        } else {
            if (joins.size() > 0) {
                joins.get(joins.size() - 1).addCondition(c);
            }
        }
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "more condition")
    public NOSQL more(String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        conditions.add((new MoreCondition(par, value.toString())).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param locale", desc = "set locale for strings")
    public NOSQL locale(String locale) {
        this.locale = locale;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "less condition")
    public NOSQL less(String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        conditions.add((new LessCondition(par, value.toString())).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "less equals condition")
    public NOSQL lessequals(String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        conditions.add((new LessEqualsCondition(par, value.toString())).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "more equals condition")
    public NOSQL moreequals(String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        conditions.add((new MoreEqualsCondition(par, value.toString())).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "like condition")
    public NOSQL like(String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        conditions.add((new LikeCondition(par, value.toString())).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "Param key, String value", desc = "like condition")
    public NOSQL likeignorecase(String key, Object value) {
        Param par = new Param(key, value.toString(), ((value instanceof String) ? "TEXT" : "NUMERIC"));
        conditions.add((new LikeIgnoreCaseCondition(par, value.toString())).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "sort column ascending")
    public NOSQL in(String field, List value) {
        Param par = new Param(field, value.toString(), "TEXT");
        conditions.add((new InCondition(par, value)).setNot(next_not).setOr(next_or));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "summary aggregate function")
    public NOSQL sum(String key) {
        aggs.add(new SumAggregate(key));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "average aggregate function")
    public NOSQL avg(String key) {
        aggs.add(new AvgAggregate(key));
        next_not = false;
        next_or = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "minimum aggregate function")
    public NOSQL min(String key) {
        aggs.add(new MinAggregate(key));
        next_not = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "maximum aggregate function")
    public NOSQL max(String key) {
        aggs.add(new MaxAggregate(key));
        next_not = false;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "", desc = "negative function")
    public NOSQL not() {
        next_not = true;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "", desc = "or condition")
    public NOSQL or() {
        next_or = true;
        //conditions.get(conditions.size()-1).setOr(true);
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "paging function, sets size of page")
    public NOSQL pagesize(int pgs) {
        pagesize = pgs;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "paging function, sets number of page")
    public NOSQL pageno(int pgs) {
        pageno = pgs;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "sort column ascending")
    public NOSQL asc(String field) {
        sorts.add(new SortAsc(field));
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "sort column descending")
    public NOSQL desc(String field) {
        sorts.add(new SortDesc(field));
        return this;
    }
    
    @JavaScriptMethod(type = "database", autocomplete = true, params = "String key", desc = "sort column descending")
    public NOSQL extractFields(Object fields) {
        if(fields instanceof List)
            this.fields.addAll((List)fields);
        else
            this.fields.add(fields.toString());
        return this;
    }

    public String formatQuery() {
        String select_clause = "select ";
        int i = 1;

        String keyfield_type = "";
        String preformat_subq = null;
        if (subquery != null) {
            preformat_subq = subquery.formatQuery();
            ArrayList<Param> tmp = new ArrayList();
            for (Param p : subquery.pars) {
                boolean skip = false;
                for (Param p1 : pars) {
                    if (p.name.equalsIgnoreCase(p1.name)) {
                        skip = true;
                    }

                }
                if (!skip) {
                    p.relation = relation;
                    tmp.add(p);
                }
            }
            pars.addAll(tmp);
        }

        for (Join j : joins) {
            NOSQL ns;
            if (j.relation == null) {
                ns = j.view;
            } else {
                ns = (new NOSQL(this.g)).get(j.relation);
            }

            for (Param p : ns.pars) {
                for (Param p1 : pars) {
                    if (p.name.equalsIgnoreCase(p1.name)) {
                        p.alias = p.relation + "$" + p.name;
                    }
                }
            }

            pars.addAll(ns.pars);
        }

        for (Param p : pars) {
            boolean is_agg = false;

            for (Aggregate agg : aggs) {
                if (agg.keyfield.equalsIgnoreCase(p.name)) {
                    select_clause += agg.toSQL();
                    if (i != pars.size()) {
                        select_clause += ",\n";
                        is_agg = true;
                        break;
                    }
                }
            }

            if (!is_agg) {
                select_clause += p.relation + "." + p.name + (p.alias == null ? "" : " as " + p.alias);
                if (i != pars.size()) {
                    select_clause += ",\n";
                }
            }

            i++;
        }

        select_clause += " from " + ((subquery != null) ? "(" + preformat_subq + ") " + relation.toLowerCase() : relation.toLowerCase());

        String join_clause = "";

        for (Join j : joins) {
            join_clause += j.toSQL(relation);
        }

        select_clause += join_clause;

        boolean has_cond = false;

        String where_clause = "";
        for (Condition c : conditions) {
            for (Param p : pars) {
                boolean is_agg = false;
                for (Aggregate agg : aggs) {
                    if (agg.keyfield.equalsIgnoreCase(p.name)) {
                        is_agg = true;
                        break;
                    }
                }

                if (!is_agg) {
                    if (p.name.equalsIgnoreCase(c.keyfield.name)) {
                        if (c.relation == null || c.relation != null && c.relation.equalsIgnoreCase(p.relation)) {
                            where_clause += c.getOr() + c.getNot() + c.toSQL(p.relation);
                            has_cond = true;
                        }
                    }
                }
            }

        }
        if (where_clause.startsWith(" or ")) {
            where_clause = where_clause.replaceFirst(" or ", " where");
        } else {
            where_clause = where_clause.replaceFirst(" and ", " where");
        }

        if (has_cond) {
            select_clause += where_clause;
        }

        String group_clause = " group by ";

        i = 1;

        boolean has_group = false;

        for (Param p : pars) {
            boolean is_agg = false;

            for (Aggregate agg : aggs) {
                if (agg.keyfield.equalsIgnoreCase(p.name)) {
                    is_agg = true;
                    has_group = true;
                    break;
                }
            }

            if (!is_agg) {
                group_clause += p.name;
                if (i != pars.size()) {
                    group_clause += ",\n";
                }

            }

            i++;
        }

        if (has_group) {
            select_clause += group_clause;
        }

        String sort_clause = " order by ";

        i = 1;

        boolean has_sort = false;

        for (Param p : pars) {

            for (Sort st : sorts) {
                if (p.name.equalsIgnoreCase(st.keyfield)) {
                    boolean is_agg = false;

                    for (Aggregate agg : aggs) {
                        if (agg.keyfield.equalsIgnoreCase(p.name)) {
                            is_agg = true;
                            break;
                        }
                    }

                    if (!is_agg) {
                        sort_clause += st.toSQL();
                        if (i != sorts.size()) {
                            sort_clause += ",\n";
                        }
                        has_sort = true;
                    }

                    i++;
                }
            }
        }

        if(sort_clause.endsWith(",\n"))
        {
             int idx = sort_clause.lastIndexOf(",\n");
             sort_clause = sort_clause.substring(0, idx);
        }
        if (has_sort) {
            select_clause += sort_clause;
        }

        if (pagesize > 0 && pageno > 0) {
            select_clause += " limit " + pagesize + " offset " + (pageno - 1) * pagesize;
        }

        LOG.info(select_clause);

        return select_clause;
    }
    
    private boolean checkField(String field)
    {
        return (fields.size() == 0)||fields.indexOf(field)>=0;
    }
            
    @JavaScriptMethod(type = "database", autocomplete = true, params = "", desc = "applies ")
    public NOSQL apply() {
        dataSet = new ArrayList();
        String select_clause = formatQuery();

        InnerDatabase h = null;

        if (ConfigUtils.getConfigBoolean("thrones.db.use_trans", false)) {
            h = MemoryDb.initInstance("");
        } else {
            if (ConfigUtils.getConfigString("thrones.db.database", "postgres").equalsIgnoreCase("postgres")) {
                h = PostgresDb.initInstance("");
            } 
            else if (ConfigUtils.getConfigString("thrones.db.database", "postgres").equalsIgnoreCase("mysql")) {
                h = MySqlDb.initInstance("");
            }
            else {
                h = new DBHelper();
            }
        }
        ResultSet rs = null;

        //Gson gson=new Gson();
        //StringWriter sw=new StringWriter();
        //JsonWriter jw=new JsonWriter(sw);
        try //make single object
        {
            //jw.beginArray();
            rs = h.runSelect(select_clause);

            try {
                for (; rs.next();) {
                    Map<String, Object> tmp = new HashMap();
                    //jw.beginObject();
                    int j = 1;
                    for (Param p : pars) {
                        if(!checkField(p.name)){
                            //do something
                        }
                        else if (p.type.equalsIgnoreCase("TEXT")) {
                            if (locale != null) {
                                if (StringUtils.isJSONValid(rs.getString(j))) {
                                    try {
                                        Map rootMap = new Gson().fromJson(rs.getString(j), Map.class);

                                        Object tmpString = rootMap.get(locale);
                                        if (tmpString != null) {
                                            tmp.put(p.name, tmpString);
                                        } else {
                                            if (System.getProperty("thrones.db.locale.default") != null) {
                                                tmpString = rootMap.get(System.getProperty("thrones.db.locale.default"));
                                                if (tmpString != null) {
                                                    tmp.put(p.name, tmpString);
                                                } else {
                                                    tmp.put(p.name, rs.getString(j));
                                                }
                                            } else {
                                                tmp.put(p.name, rs.getString(j));
                                            }
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                        tmp.put(p.name, rs.getString(j));
                                    }
                                } else {
                                    tmp.put(p.name, rs.getString(j));
                                }
                            } else {
                                tmp.put(p.name, rs.getString(j));
                            }
                            //gson.toJson(rs.getString(j), String.class, jw.name(p.name));
                        } else if (p.type.equalsIgnoreCase("COMPLEX")) {
                            try {
                                Map<String, Object> RootMapObject = new Gson().fromJson(rs.getString(j), Map.class);
                                tmp.put(p.name, RootMapObject);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            //gson.toJson(RootMapObject, Map.class, jw.name(p.name));
                        } else if (p.type.equalsIgnoreCase("LIST")) {
                            try {
                                List<Object> RootMapObject = new Gson().fromJson(rs.getString(j), List.class);
                                tmp.put(p.name, RootMapObject);
                            } catch (Exception ex) {
                                java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, "error converting list for param: " + p.name, ex);
                            }
                            //gson.toJson(RootMapObject, List.class, jw.name(p.name));
                        }
                        else if (p.type.equalsIgnoreCase("INT")){
                            try
                            {
                                tmp.put(p.name, rs.getInt(j));
                            }
                            catch(Exception Ex)
                            {
                                Ex.printStackTrace();
                            }
                            //gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                        }
                        else if (p.type.equalsIgnoreCase("BOOLEAN")){
                            try
                            {
                                tmp.put(p.name, rs.getBoolean(j));
                            }
                            catch(Exception Ex)
                            {
                                Ex.printStackTrace();
                            }
                            //gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                        }
                        else {
                            try
                            {
                                tmp.put(p.name, rs.getDouble(j));
                            }
                            catch(Exception Ex)
                            {
                                Ex.printStackTrace();
                            }
                            //gson.toJson(rs.getDouble(j), Double.class, jw.name(p.name));
                        }
                        j++;
                    }
                    dataSet.add(tmp);
                    //jw.endObject();

                }

                rs.close();
                h.closeDatabase();
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(Serializer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(NOSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOG.info("length of result of query is " + dataSet.size());
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String divide_field, String avg_fieldname, Double parts", desc = "window function sample_avg")
    public NOSQL sampleAvg(final String divide_field, String avg_fieldname, Double parts) {
        List<Map<String, Object>> result = new ArrayList();

        Collections.sort(dataSet, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> lhs, Map<String, Object> rhs) {
                Object testField1 = lhs.get(divide_field);
                Object testField2 = rhs.get(divide_field);
                if (testField1 instanceof Integer) {
                    return (Integer) testField1 > (Integer) testField2 ? -1 : ((Integer) testField1 < (Integer) testField2) ? 1 : 0;
                } else if (testField2 instanceof Double) {
                    return (Double) testField1 > (Double) testField2 ? -1 : ((Double) testField1 < (Double) testField2) ? 1 : 0;
                } else if (testField2 instanceof Long) {
                    return (Long) testField1 > (Long) testField2 ? -1 : ((Long) testField1 < (Long) testField2) ? 1 : 0;
                } else {
                    return 0;
                }
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                //return lhs.customInt > rhs.customInt ? -1 : (lhs.customInt < rhs.customInt) ? 1 : 0;
            }
        });

        if (dataSet.size() < parts.intValue()) {
            parts = (double) dataSet.size();
        }

        int page_size = dataSet.size() / parts.intValue();
        for (int i = 0; i < parts; i++) {
            List<Map<String, Object>> subset = dataSet.subList(i * page_size, (i + 1) * page_size);
            Map<String, Object> base_row = subset.get(0);
            Object test_agg_value = base_row.get(avg_fieldname);
            Object agg_value = null;
            for (Map<String, Object> row : subset) {
                if (test_agg_value instanceof Double) {
                    if (agg_value == null) {
                        agg_value = new Double((Double) row.get(avg_fieldname));
                    } else {
                        agg_value = (Double) agg_value + (Double) row.get(avg_fieldname);
                    }
                } else if (test_agg_value instanceof Integer) {
                    if (agg_value == null) {
                        agg_value = new Integer((Integer) row.get(avg_fieldname));
                    } else {
                        agg_value = (Integer) agg_value + (Integer) row.get(avg_fieldname);
                    }
                } else if (test_agg_value instanceof Long) {
                    if (agg_value == null) {
                        agg_value = new Long((Long) row.get(avg_fieldname));
                    } else {
                        agg_value = (Long) agg_value + (Long) row.get(avg_fieldname);
                    }
                } else {
                    agg_value = null;
                }
            }

            if (test_agg_value instanceof Double) {
                agg_value = (Double) agg_value / subset.size();
            } else if (test_agg_value instanceof Integer) {
                agg_value = (Integer) agg_value / subset.size();
            } else if (test_agg_value instanceof Long) {
                agg_value = (Long) agg_value / subset.size();
            } else {
                agg_value = null;
            }

            base_row.put(avg_fieldname, agg_value);
            result.add(base_row);
        }

        dataSet = result;
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String fieldname, String partition, String alias, Double parts", desc = "window function ntile")
    public NOSQL ntile(String fieldname, String partition, String alias, Double parts) {
        throw new UnsupportedOperationException("Not supported yet.");
        //return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String fieldname, String inside_name", desc = "extracts field from nested object as field of query")
    public NOSQL extractField(String fieldname, String inside_name) {
        for (Map<String, Object> ds : dataSet) {
            if (ds.get(fieldname) instanceof Map) {
                Map<String, Object> tmp = (Map) ds.get(fieldname);
                ds.put(inside_name, tmp.get(inside_name));
            }
        }
        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "String fieldname, String partition, String alias", desc = "sum window function ")
    public NOSQL sum(String fieldname, String partition, String alias) {
        if (alias == null) {
            alias = "sum_" + fieldname + "_over_" + partition;
        }
        if (1 == 1) {
            Map<Object, Double> map_over = new HashMap();
            Object part;
            for (Map<String, Object> ds : dataSet) {
                Double val = map_over.get(ds.get(fieldname));
                part = partition == null ? "*" : ds.get(partition);
                if (val == null) {
                    map_over.put(part, 0.0);
                    val = 0.0;
                }

                if (ds.get(fieldname) instanceof String) {
                    try {
                        map_over.put(part, val + Double.parseDouble(ds.get(fieldname).toString()));
                    } catch (Exception Ex) {
                    }
                } else if (ds.get(fieldname) instanceof Integer) {
                    try {
                        map_over.put(part, val + ((Integer) ds.get(fieldname)).doubleValue());
                    } catch (Exception Ex) {
                    }
                } else if (ds.get(fieldname) instanceof Double) {
                    map_over.put(part, val + ((Double) ds.get(fieldname)));
                }
            }

            for (Map<String, Object> ds : dataSet) {
                ds.put(alias, map_over.get(partition == null ? "*" : ds.get(partition)));
            }
        } else {

        }

        return this;
    }

    @JavaScriptMethod(type = "database", autocomplete = true, params = "", desc = "retrieves result as Object")
    public Object getResult() {
        Object result = ObjectUtils.convert(dataSet);
        return result;
    }

}
