/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.conditions;

import ru.wnfx.scriptfighter.db.core.Param;

/**
 *
 * @author sergk
 */
public abstract class Condition {

    public Param keyfield;
    public boolean not;
    public boolean or;
    public String relation;

    public abstract String toSQL(String relation);

    protected Condition() {
        not = false;
        or = false;
    }

    public Condition setNot(boolean is_not) {
        not = is_not;
        return this;
    }

    public Condition setOr(boolean is_or) {
        or = is_or;
        return this;
    }

    public String getNot() {
        return not ? " not " : "";
    }

    public String getOr() {
        return or ? " or " : " and ";
    }

    public abstract String toJoin(String relation1, String relation2);
}
