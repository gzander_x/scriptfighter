/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.conditions;

import java.util.List;
import ru.wnfx.scriptfighter.db.core.Param;
import ru.wnfx.scriptfighter.utils.StringUtils;

/**
 *
 * @author sergk
 */
public class InCondition extends Condition {

    public List<String> value;

    public InCondition(Param keyfield, List value) {
        this.keyfield = keyfield;
        this.value = value;//
    }

    @Override
    public String toSQL(String relation) {
        String select_clause = "";
        if (value == null) {
            select_clause += " " + relation + "." + keyfield.name + " is null";
        } else if (value.size() == 0) {
            select_clause += " " + relation + "." + keyfield.name + " is null";
        } else {
            if (keyfield.name.length() > 0) {
                String comma = "";//(keyfield.type.equalsIgnoreCase("TEXT")||keyfield.type.equalsIgnoreCase("COMPLEX")||keyfield.type.equalsIgnoreCase("COMPLEX"))?"'":"";

                for (String s : value) {
                    if (!StringUtils.isNumeric(s) && !StringUtils.isInteger(s)) {
                        comma = "'";
                    }
                }
                select_clause += " " + relation + "." + keyfield.name + " in (" + comma + String.join(comma + "," + comma, value) + comma + ")";
            }
        }
        return select_clause;
    }

    @Override
    public String toJoin(String relation1, String relation2) {
        String select_clause = "";
        if (value == null) {
            select_clause += " " + relation1 + "." + keyfield.name + " is null";
        } else {
            if (keyfield.name.length() > 0) {
                select_clause += " " + relation1 + "." + keyfield.name + "<" + relation2 + "." + value;
            }
        }
        return select_clause;
    }
}
