/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.aggregates;

/**
 *
 * @author sergk
 */
public abstract class Aggregate 
{
    public String keyfield;
    public abstract String toSQL();
}
