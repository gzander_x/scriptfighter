/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.db.persistent.queue;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author sergk
 */
public class Transaction implements Serializable
{
    public String transactionId;
    public int update_count;
    public String statementText;
    private static final long serialVersionUID = 7222888295622773337L;
    
    public Transaction()
    {
        transactionId = java.util.UUID.randomUUID().toString().replace("-", "");
    }
    
    public Transaction(String statement, int update_count)
    {
        transactionId = java.util.UUID.randomUUID().toString().replace("-", "");
        statementText=statement;
        this.update_count = update_count;
    }
    
    /**
   * Always treat de-serialization as a full-blown constructor, by
   * validating the final state of the de-serialized object.
   */
   private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException 
   {
     //always perform the default de-serialization first
     aInputStream.defaultReadObject();
  }

    /**
    * This is the default implementation of writeObject.
    * Customise if necessary.
    */
    private void writeObject(ObjectOutputStream aOutputStream) throws IOException 
    {
      //perform the default serialization for all non-transient, non-static fields
      aOutputStream.defaultWriteObject();
    }
}
