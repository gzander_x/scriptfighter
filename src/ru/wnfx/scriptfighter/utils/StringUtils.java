/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author sergk
 */
public class StringUtils {

    public static final Logger LOG = Logger.getLogger(StringUtils.class);
    private static final Gson gson = new Gson();

    public static String Capitalize(String input) {
        return input == null ? null : (input.substring(0, 1).toUpperCase() + input.substring(1));
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean isInteger(String str) {
        try {
            int d = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static int tryInteger(String val) {
        if (isInteger(val)) {
            return Integer.parseInt(val);
        }
        if (isNumeric(val)) {
            double d = Double.parseDouble(val);
            if (Math.ceil(d) == d) {
                return (int) d;
            } else {
                throw new NumberFormatException();
            }
        }
        throw new NumberFormatException();
    }

    public static short tryShort(String val) {
        if (isInteger(val)) {
            return Short.parseShort(val);
        }
        if (isNumeric(val)) {
            double d = Double.parseDouble(val);
            if (Math.ceil(d) == d) {
                return (short) d;
            } else {
                throw new NumberFormatException();
            }
        }
        throw new NumberFormatException();
    }

    public static boolean isJSONValid(String JSON_STRING) {

        /*if(JSON_STRING==null)
          return false;
      if(JSON_STRING.length()==0)
          return false;*/
        try {
            gson.fromJson(JSON_STRING, Map.class);
            return true;
        } catch (com.google.gson.JsonSyntaxException ex) {
            try {
                gson.fromJson(JSON_STRING, Set.class);
                return true;
            } catch (com.google.gson.JsonSyntaxException ex1) {
                return false;
            }
        }
    }

    public static String[] getURLParts(String URL) {
        int start = 0;
        int finish = 0;
        int count_tags = (URL.length() - URL.replace("%", "").length()) / 2;
        String[] result = new String[count_tags];
        for (int i = 0; i < count_tags; i++) {
            start = URL.indexOf("%", finish);
            finish = URL.indexOf("%", start + 1);
            result[i] = URL.substring(start + 1, finish);
            finish++;
        }
        return result;
    }

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String MD5(String md5, String encoding) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = null;
            try {
                array = md.digest(md5.getBytes(encoding));
            } catch (UnsupportedEncodingException ex) {
                java.util.logging.Logger.getLogger(StringUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String getHash(String message, String algorithm) {
        try {
            byte[] buffer = message.getBytes();
            MessageDigest md = MessageDigest.getInstance(algorithm);
            md.update(buffer);
            byte[] digest = md.digest();
            String hex = "";
            for (int i = 0; i < digest.length; i++) {
                int b = digest[i] & 0xff;
                if (Integer.toHexString(b).length() == 1) {
                    hex = hex + "0";
                }
                hex = hex + Integer.toHexString(b);
            }
            return hex;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    static public String join(List<String> list, String conjunction) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String item : list) {
            if (first) {
                first = false;
            } else {
                sb.append(conjunction);
            }
            sb.append(item);
        }
        return sb.toString();
    }

    public static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static String urlEncodeUTF8(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }

    public static String listToCSV(List<Map<?, ?>> list) {
        StringBuilder sb = new StringBuilder();
        if (list.size() > 0) {
            Map<String, Object> first = (Map<String, Object>) list.get(0);
            for (String key : first.keySet()) {
                if (sb.length() > 0) {
                    sb.append(";");
                }
                sb.append(key);
            }
            sb.append(System.getProperty("line.separator"));

            for (Map<?, ?> entry : list) {
                if (sb.length() > 0) {
                    sb.append(mapToCSV(entry));
                }
                sb.append(System.getProperty("line.separator"));
            }
        }
        return sb.toString();
    }

    public static String listToCSVExt(List<Map<?, ?>> list, Map<String, String> fields) {
        StringBuilder sb = new StringBuilder();
        if (list.size() > 0) {
            LinkedHashMap<String, String> lm = new LinkedHashMap();

            for (Entry<String, String> key : fields.entrySet()) {
                lm.put(key.getKey(), key.getValue());
            }
            for (Entry<String, String> key : lm.entrySet()) {
                if (sb.length() > 0) {
                    sb.append(";");
                }
                sb.append(key.getValue());
            }
            sb.append(System.getProperty("line.separator"));

            for (Map<?, ?> entry : list) {
                if (sb.length() > 0) {
                    LinkedHashMap<Object, Object> tmp_map = new LinkedHashMap();
                    for (Entry<String, String> kkk : lm.entrySet()) {
                        tmp_map.put(kkk.getKey(), entry.get(kkk.getKey()));
                    }
                    sb.append(mapToCSV(tmp_map));
                }
                sb.append(System.getProperty("line.separator"));
            }
        }
        return sb.toString();
    }

    public static String mapToCSV(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(";");
            }
            sb.append(entry.getValue() == null ? "" : entry.getValue().toString().replaceAll(";", ","));
        }
        return sb.toString();
    }

    public static int randomInt(int min, int max) {
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        return randomNum;
    }

    public static byte[] compressGZIP(final String str) throws IOException {
        if ((str == null) || (str.length() == 0)) {
            return null;
        }
        ByteArrayOutputStream obj = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(obj);
        gzip.write(str.getBytes("UTF-8"));
        gzip.flush();
        gzip.close();
        return obj.toByteArray();
    }

    public static String decompressGZIP(final byte[] compressed) throws IOException {
        final StringBuilder outStr = new StringBuilder();
        if ((compressed == null) || (compressed.length == 0)) {
            return "";
        }
        if (isCompressed(compressed)) {
            final GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(compressed));
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gis, "UTF-8"));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                outStr.append(line);
            }
        } else {
            outStr.append(compressed);
        }
        return outStr.toString();
    }

    public static boolean isCompressed(final byte[] compressed) {
        return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
    }

    private static final String HTML_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
    private static Pattern pattern = Pattern.compile(HTML_PATTERN);

    public static boolean hasHTMLTags(String text) {
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static String cutLog(String script_body, String script_name) {
        return script_body.replaceAll(Pattern.quote("$log("), "console.log('" + script_name + ":'+__LINE__+': '+");
    }

    public static String ellipsize(String text, int max) {

        if (text.length() <= max) {
            return text;
        }

        // Start by chopping off at the word before max
        // This is an over-approximation due to thin-characters...
        return text.substring(0, max) + "...";
    }

    public static Map<String, String> queryToMap(String query) {
        Map<String, String> result = new HashMap();
        for (String param : query.split("&")) {
            String[] entry = param.split("=");

            if (entry.length > 1) {
                if (!entry[0].contains("?") && !entry[1].contains("?")) {
                    result.put(entry[0], entry[1]);
                }
            } else {
                if (!entry[0].contains("?")) {
                    result.put(entry[0], "");
                }
            }
        }
        return result;
    }
    
    public static String queryToJson(String query) {
        String result = gson.toJson(queryToMap(query), Map.class);
        return result;
    }

    public static boolean checkUrlQuery(String q) {
        //return queryToMap(q).size()>0;
        return false;
    }

    public static String paramJson(String paramIn) {
        paramIn = paramIn.replaceAll("=", "\":\"");
        paramIn = paramIn.replaceAll("&", "\",\"");
        return "{\"" + paramIn + "\"}";
    }

    public static String padLeftZeros(String inputString, int length) {
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }

    public static String convertStreamToString(InputStream is)
            throws IOException {
        /*
             * To convert the InputStream to String we use the
             * Reader.read(char[] buffer) method. We iterate until the
    35.         * Reader return -1 which means there's no more data to
    36.         * read. We use the StringWriter class to produce the string.
    37.         */
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    public static byte[] convertStreamToByteArray(InputStream is) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];
        try {
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
        } catch (IOException e) {
        }

        return buffer.toByteArray();
    }

    public static String SHA256(String s) {
        return org.apache.commons.codec.digest.DigestUtils.sha256Hex(s);
    }
    
}
