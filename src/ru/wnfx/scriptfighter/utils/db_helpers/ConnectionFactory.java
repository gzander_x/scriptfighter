package ru.wnfx.scriptfighter.utils.db_helpers;

import java.sql.*;
import org.apache.log4j.*;



/**
 * Класс реализует коннект БД.
 * @author Смагин Сергей
 */
public class ConnectionFactory {

        public static final Logger LOG=Logger.getLogger(ConnectionFactory.class);

	private String driver;
	private String cnString;
	private String user;
	private String password;
	public Connection connection;
        PreparedStatement stCheck;
	String txtCheck = "SELECT 0 FROM DUAL";


	public ConnectionFactory (String driver, String cnString, String user, String password, String sql) {
		this.driver = driver; //"oracle.jdbc.OracleDriver";
		this.cnString = cnString; //"jdbc:oracle:thin:@192.168.0.27:1521:FIRENDMS";
		this.user = user; //"ccbo_new";
		this.password = password; //"ccbo_new";
                LOG.debug("Factory created");
	}

	/** Устанавлиает соединение с базой данных. */
	public void connect () throws DB_Exception {
		try {
			Class.forName (this.driver);
			this.connection = DriverManager.getConnection (this.cnString, this.user, this.password);
			//this.stDetalization = this.connection.prepareStatement (this.txtDetalization);
			this.stCheck = this.connection.prepareStatement (this.txtCheck);
                        LOG.debug("connect passed");
		} catch (ClassNotFoundException cnfe) {
                        LOG.error("Не найден файл драйвера: "+cnfe.getMessage());
			throw new DB_Exception ("Не найден файл драйвера: " + this.driver);
		} catch (SQLException sqle) {
                        LOG.error("Невозможно соединиться с базой данных: "+sqle.getMessage());
			throw new DB_Exception ("Невозможно соединиться с базой данных: " + sqle.getMessage ());
		}
	}

}

