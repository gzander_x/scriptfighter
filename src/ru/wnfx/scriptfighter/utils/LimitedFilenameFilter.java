/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;
import java.io.File;
import java.io.FilenameFilter;
import org.apache.log4j.Logger;
/**
 *
 * @author sergk
 */
public class LimitedFilenameFilter implements FilenameFilter
{
    public static final String[] ext_array={"jpg","png","gif","bmp"};
    public static final Logger LOG = Logger.getLogger(LimitedFilenameFilter.class);
    
    public int comparison_counter;
    public int limit;
    public int offset;
    public boolean is_video;
    
    public LimitedFilenameFilter(int limit, int offset, boolean is_video)
    {
        this.limit=limit;
        this.offset=offset;
        comparison_counter=0;
        this.is_video=is_video;
    }
    
    @Override
    public boolean accept(File dir, String name) 
    {
        if(is_video)
        {
            LOG.debug("check video");
            if(name.endsWith(".vdata"))
            {
                if(comparison_counter>=offset && comparison_counter<(offset+limit))
                {
                    comparison_counter++;
                    LOG.debug("true. check_file: "+name+", counter: "+comparison_counter+", limit: "+limit+", offset: "+offset);
                    return true;
                }
                else
                {
                    comparison_counter++;
                    LOG.debug("false. check_file: "+name+", counter: "+comparison_counter+", limit: "+limit+", offset: "+offset);
                    return false;
                }
            }
        }
        else
        {
            LOG.debug("check photo");
            for(int i=0; i<ext_array.length; i++)
            {
                //LOG.info("check_file: "+name);
                if(name.endsWith("_thumb."+ext_array[i]))
                {
                    if(comparison_counter>=offset && comparison_counter<(offset+limit))
                    {
                        comparison_counter++;
                        LOG.debug("true. check_file: "+name+", counter: "+comparison_counter+", limit: "+limit+", offset: "+offset);
                        return true;
                    }
                    else
                    {
                        comparison_counter++;
                        LOG.debug("false. check_file: "+name+", counter: "+comparison_counter+", limit: "+limit+", offset: "+offset);
                        return false;
                    }
                }
            }
        }
        //comparison_counter++;
        return false;
    }
}
