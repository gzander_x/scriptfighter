/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.wnfx.scriptfighter.utils;

import com.google.gson.stream.JsonWriter;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Bindings;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import ru.wnfx.scriptfighter.compatible.JavaScriptArray;
//import jdk.nashorn.api.scripting.ScriptObjectMirror;
//import ru.wnfx.scriptfighter.engine.core.ThreadsHolder;
import sun.org.mozilla.javascript.internal.NativeArray;
import sun.org.mozilla.javascript.internal.NativeObject;

/**
 *
 * @author sergk
 */
public class ObjectUtils {

    public static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ObjectUtils.class);
    public static final String engine_type = ConfigUtils.getConfigString("thrones.engine.type", "rhino");

    public static class NeedStreamingException extends Throwable {
    }

    public static Map<String, Object> getMapFromNative(NativeObject obj) {
        HashMap<String, Object> mapParams = new HashMap<String, Object>();

        if (obj != null) {
            Object[] propIds = NativeObject.getPropertyIds(obj);
            for (Object propId : propIds) {
                String key = propId.toString();
                Object value = NativeObject.getProperty(obj, key);
                mapParams.put(key, value);
            }
        }

        return mapParams;
        //work with mapParams next..
    }

    public static Object convert(final Object obj) {
        try {
            return convertx(obj, false);
        } catch (NeedStreamingException ex) {
            return null;
        }
    }

    public static Object convertThrows(final Object obj) throws NeedStreamingException {

        return convertx(obj, true);
    }

    public static Object convertx(final Object obj, boolean need_throw) throws NeedStreamingException {
        if (obj instanceof List && need_throw) {
            if (((List) obj).size() > 10000) {
                throw new NeedStreamingException();
            }
        }

        if (obj instanceof Bindings) {
            try {
                final Class<?> cls = Class.forName("jdk.nashorn.api.scripting.ScriptObjectMirror");

                if (cls.isAssignableFrom(obj.getClass())) {
                    final Method isArray = cls.getMethod("isArray");
                    final Object result = isArray.invoke(obj);
                    if (result != null && result.equals(true)) {
                        final Method values = cls.getMethod("values");
                        final Object vals = values.invoke(obj);
                        if (vals instanceof Collection<?>) {
                            final Collection<?> coll = (Collection<?>) vals;
                            //return coll.toArray(new Object[0]);
                            List list;
                            if (coll instanceof List) {
                                list = (List) coll;
                            } else {
                                list = new ArrayList(coll);
                            }

                            List finalList = new ArrayList();
                            for (Object oo : list) {
                                finalList.add(ObjectUtils.convertx(oo, need_throw));
                            }

                            return finalList;
                        }
                    } else {
                        final Method keySet = cls.getMethod("keySet");
                        final Object keys = keySet.invoke(obj);
                        Map<String, Object> resultMap = new HashMap();
                        if (keys != null && !((Set) keys).isEmpty()) {
                            final Set<String> ks = (Set) keys;

                            for (String oo : ks) {
                                if (oo.length() > 0) {
                                    resultMap.put(oo, ObjectUtils.convertx(((Map) obj).get(oo), need_throw));
                                }
                            }
                        }

                        return resultMap;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (obj instanceof NativeObject) {
            return getMapFromNative((NativeObject) obj);
        } else if (obj instanceof NativeArray) {
            return getListFromNative((NativeArray) obj);
        }
        return obj;
    }

    public static List<Object> getListFromNative(Object obj) {
        
        if(obj instanceof NativeArray)
        {
            List<Object> result = new ArrayList();

            if (obj != null) {
                Object[] propIds = NativeObject.getPropertyIds(((NativeArray)obj));
                for (Object o : ((NativeArray)obj).getIds()) {
                    int index = (Integer) o;
                    Object value = ((NativeArray)obj).get(index, null);
                    result.add(value);
                }

            }

            return result;
        }
        return (List<Object>) obj;
        //work with mapParams next..
    }
    
    public static List<Object> getList(Object obj)
    {
        if(obj instanceof List)
        {
            List<Object> result = new ArrayList();
            for(Object inner_obj: (List)obj)
            {
                result.add(inner_obj);
            }
            return result;
        }
        else
          return (List<Object>)obj; 
    }

    public static Map<String, Object> getMap(Object o) {
        Map<String, Object> result = new HashMap<String, Object>();
        Field[] declaredFields = o.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            try {
                result.put(field.getName(), field.get(o));
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    private static Map<String, Object> _introspect(Object obj) {
        Map<String, Object> result = new HashMap<String, Object>();
        BeanInfo info = null;
        try {
            info = Introspector.getBeanInfo(obj.getClass());
        } catch (IntrospectionException ex) {
            Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            Method reader = pd.getReadMethod();
            if (reader != null) {
                try {
                    result.put(pd.getName(), reader.invoke(obj));
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(ObjectUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return result;
    }

    public static Map<String, Object> introspect(Object obj) {
        Map<String, Object> result = getMap(obj);
        result.putAll(_introspect(obj));
        return result;
    }

    public static Object checkList(Object intake) {
        if (intake instanceof ScriptObjectMirror) {
            if (intake instanceof Map) {
                Map checker = (Map<Object, Object>) intake;
                if (checker.size() > 0) {
                    for (Object key : checker.keySet()) {
                        if (key instanceof String) {
                            if (!isInteger(key.toString())) {
                                return intake;
                            }
                        } else {
                            return intake;
                        }
                    }
                    // all keys is stringified integers
                    List<Object> result = new ArrayList();
                    for (Object value : checker.values()) {
                        result.add(value);
                    }

                    return result;
                } else {
                    return intake;
                }
            } else {
                return intake;
            }
        } else {
            return intake;
        }
    }

    public static boolean isInteger(String s) {
        return isInteger(s, 10);
    }

    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty()) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '-') {
                if (s.length() == 1) {
                    return false;
                } else {
                    continue;
                }
            }
            if (Character.digit(s.charAt(i), radix) < 0) {
                return false;
            }
        }
        return true;
    }

    public static Object listToNativeArray(List l) {
        if (l != null) {
            if (engine_type.equalsIgnoreCase("rhino")) {
                NativeArray nobj = new NativeArray(l.size());
                int i = 0;
                for (Object entry : l) {
                    nobj.put(i, nobj, entry);
                    i++;
                }
                return nobj;
            } else if (engine_type.equalsIgnoreCase("nashorn")) {
                return JavaScriptArray.toJSArray((ArrayList) l);
            }

        }
        return null;

    }

    public static Object mapToNativeObect(Map<String, Object> l) {
        if (l != null) {
            if (engine_type.equalsIgnoreCase("rhino")) {
                NativeObject nobj = new NativeObject();
                for (Map.Entry<String, Object> entry : l.entrySet()) {
                    nobj.defineProperty(entry.getKey(), entry.getValue(), NativeObject.READONLY);
                }
                return nobj;
            } else if (engine_type.equalsIgnoreCase("nashorn")) {
                return l;
            }

        }
        return null;

    }

    public static Object convertNative(Object obj) {
        if (obj instanceof Map) {
            return mapToNativeObect((Map) obj);
        } else if (obj instanceof List) {
            return listToNativeArray((List) obj);
        } else {
            return JavaScriptArray.toJSArray((ArrayList) obj);
        }
    }

    public static void streamSerializer(JsonWriter sw, JsonWriter cache_writer, Object o, String property) throws IOException {
        
        o = checkList(o);
        if (o instanceof Map) {
            Map<String, Object> tmp = (Map<String, Object>) o;
            if (property != null) {
                sw.name(property).beginObject();
                if (cache_writer != null) {
                    cache_writer.name(property).beginObject();
                }
            } else {
                sw.beginObject();
                if (cache_writer != null) {
                    cache_writer.beginObject();
                }
            }
            for (String prop : tmp.keySet()) {
                streamSerializer(sw, cache_writer, tmp.get(prop), prop);
            }
            sw.endObject();
            if (cache_writer != null) {
                cache_writer.endObject();
            }
        } else if (o instanceof List) {
            List<Object> tmp = (List<Object>) o;
            if (property != null) {
                sw.name(property).beginArray();
                if (cache_writer != null) {
                    cache_writer.name(property).beginArray();
                }
            } else {
                sw.beginArray();
                if (cache_writer != null) {
                    cache_writer.beginArray();
                }
            }

            for (Object prop : tmp) {
                streamSerializer(sw, cache_writer, prop, null);
            }
            sw.endArray();
            if (cache_writer != null) {
                cache_writer.endArray();
            }
        } else if (o instanceof String) {
            if (property != null) {
                sw.name(property).value(o.toString());
                if (cache_writer != null) {
                    cache_writer.name(property).value(o.toString());
                }
            } else {
                sw.value(o.toString());
                if (cache_writer != null) {
                    cache_writer.value(o.toString());
                }
            }
        } else if (o instanceof Number) {
            try
            {
                if (property != null) {
                    sw.name(property).value((Number) o);
                    if (cache_writer != null) {
                        cache_writer.name(property).value((Number) o);
                    }
                } else {
                    sw.value((Number) o);
                    if (cache_writer != null) {
                        cache_writer.value((Number) o);
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                cache_writer.value((Number) null);
            }
        } else if (o instanceof Double) {
            if (property != null) {
                sw.name(property).value((Double) o);
                if (cache_writer != null) {
                    cache_writer.name(property).value((Double) o);
                }
            } else {
                sw.value((Double) o);
                if (cache_writer != null) {
                    cache_writer.value((Double) o);
                }
            }
        } else if (o instanceof Long) {
            if (property != null) {
                sw.name(property).value((Long) o);
                if (cache_writer != null) {
                    cache_writer.name(property).value((Long) o);
                }
            } else {
                sw.value((Long) o);
                if (cache_writer != null) {
                    cache_writer.value((Long) o);
                }
            }
        } else if (o instanceof Boolean) {
            if (property != null) {
                sw.name(property).value((Boolean) o);
                if (cache_writer != null) {
                    cache_writer.name(property).value((Boolean) o);
                }
            } else {
                sw.value((Boolean) o);
                if (cache_writer != null) {
                    cache_writer.value((Boolean) o);
                }
            }
        } else if (o == null) {
            if (property != null) {
                sw.name(property).nullValue();
                if (cache_writer != null) {
                    cache_writer.name(property).nullValue();
                }
            } else {
                sw.nullValue();
                if (cache_writer != null) {
                    cache_writer.nullValue();
                }
            }
        } else {
            if (property != null) {
                sw.name(property).value(o.toString());
                if (cache_writer != null) {
                    cache_writer.name(property).value(o.toString());
                }
            } else {
                sw.value(o.toString());
                if (cache_writer != null) {
                    cache_writer.value(o.toString());
                }
            }
        }
    }
}
